up:
	docker-compose up -d --build

down:
	docker-compose down

php:
	docker exec -it clffony_php-fpm /bin/bash
