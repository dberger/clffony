# ClfFony

Cette application permet de 

* Lister les lignes de bus et la ligne de tram (ajouter via backoffice ou directement via parsing du site T2C) 

* Rechercher des itinéraires entre 2 arrêts avec un choix de date et d'heure (requete API XML T2C), si pas de date et d'heure renseigné on affiche le prochain a partir de l'heure actuelle

* Si connecté l'utilisateur peut garder en mémoire un arrêt A et un arrêt B et faire une recherche rapide d'itinéraire (on appelera ça des favoris)

* L'utilisateur pourra ajouter ou supprimer autant de favoris qu'il le souhaite, à partir de l'onglet favoris.

* Si le temps le permet une catégorie statistiques sera mise en place, à définir pour le moment ... 

* Outils mis en place : Docker, Symfony 4.2, PHP 7.3, Tests unitaires & Fonctionnels, Fixtures, Yarn, Webpack ...

* Utilisation d'un template Bootstrap 4 

## Installation

[Commandes](https://gitlab.com/dberger/clffony/blob/master/app/README.md)

## Conception

### Diagramme

[Diagramme de Classe](https://gitlab.com/dberger/clffony/blob/master/conception/clffony.pdf)

### Suivi des tâches

[Trello](https://trello.com/b/5dLWRRri/clffony)

