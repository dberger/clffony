# Commandes

## Docker

`make up` ou `docker-compose up --build -d` à la racine du projet

`make php` ou `docker exec -it clffony_php-fpm /bin/bash` pour se connecter au container et lancer les commandes ci-dessous
 
## Lancer les tests unitaires avec coverage

`php bin/phpunit --coverage-html ./public/coverage`

## Installer la base de données

`php bin/console d:m:m`

## Fixtures 

`php bin/console d:f:l --append`

## Installer l'application

`composer install && yarn`

## Compiler les JS et CSS

### Dev

`yarn encore dev --watch`

### Prod

`yarn encore production`

## Create admin user

`php bin/console user:create`

`php bin/console user:role:add`

## Compte admin par défaut

`admin@clffony.fr`
`R^DcXznsXu73uFkZ`

## Accès à l'application

Le serveur nginx tourne sur le port 5000 et phpmyadmin sur le port 8080.


(localhost:5000 & localhost:8080)
