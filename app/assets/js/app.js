var $ = require('jquery');
global.$ = $;
global.jQuery = $;
require('../scss/app.scss');
require('../scss/template/style.scss');
require('bootstrap');
require('select2');
require('admin-lte');
require('jquery.countdown');
require('datatables/media/js/jquery.dataTables.min');
require('datatables/media/css/jquery.dataTables.min.css');
require('font-awesome/scss/font-awesome.scss');

var app = {};
global.app = app;

app.initApp = function (fromModal = false) {
    app.initSelect2();
    if (!fromModal) {
        app.initAjaxReload();
    }
    app.initFlashMessages(); // Flash messages
};


/**
 * Init Flash Messages
 */
app.initFlashMessages = function () {
    $('.flash-messages').hide().removeClass('hidden').delay(1000).fadeIn(600).delay(6000).fadeOut(600);

    $('.flash-messages .close').unbind('click');
    $('.flash-messages .close').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parent().fadeOut(300);
    });
};

/**
 * Init Select2
 */
app.initSelect2 = function () {
    $('.select2').select2();
};

app.initHomeRouteFinderBtn = function () {
    $('.btn-find-route').on('click', function (e) {
        if ($('.select-start').val() !== '' && $('.select-end').val() !== '') {
            e.preventDefault();
            if($('.search-max-result').val() <= 0){
                app.addFlash('error', 'Le nombre de résultat doit être > 0 ');
                return;
            }
            let $searchSpinner = $('.search-spinner');
            let spinnerUrl = $searchSpinner.attr('spinner-url');
            //SpinnerconvertResultsToRouteObject
            $searchSpinner.html('<img class="spinner-modal" src="' + spinnerUrl + '"/>');
            let $divResult = $('.route-result');
            let $form = $('form');
            $divResult.slideUp();
            var url = $form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(),
                success: function (data) {
                    app.ajaxHandleCallbacks(data);
                    $divResult.html(data);
                    $divResult.slideDown();
                    app.initCountDown();
                    $searchSpinner.html('');
                }
            });
        }
    });
};

app.initCountDown = function () {
    $('.countdown').each(function () {
        $(this).countDown({
            label_dd: '',
            label_hh: '',
            label_mm: '',
            label_ss: '',
        });
        $(this).on('time.elapsed', function () {
            $(this).html('N\'est plus disponible');
        });
    });
};

/**
 * Get config DataTables
 * @returns {object}
 */
app.getConfigDatatable = function () {
    return {
        "sProcessing": "Traitement en cours...",
        "sSearch": "Rechercher&nbsp;:",
        "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
        "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
        "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoPostFix": "",
        "sLoadingRecords": "Chargement en cours...",
        "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
        "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
        "oPaginate": {
            "sFirst": "Premier",
            "sPrevious": "Pr&eacute;c&eacute;dent",
            "sNext": "Suivant",
            "sLast": "Dernier"
        },
        "oAria": {
            "sSortAscending": ": activer pour trier la colonne par ordre croissant",
            "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
        },
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": ['sorting-disabled']
        }],
        "fnDrawCallback": function () {
            app.initBackUserPageBtn();
        },
        "fnInfoCallback": function () {
            app.initBackUserPageBtn();
        },
    };
};

app.initOneDataTable = function (datatable) {
    let dataTableLangageParams = app.getConfigDatatable();

    let bLengthChange = datatable.attr('data-length-change') ? (that.attr('data-length-change') == "false" ? false : true) : true;
    let bPaginate = datatable.attr('data-paginate') ? (that.attr('data-paginate') == "false" ? false : true) : true;
    let ordering = datatable.attr('data-paginate') ? (that.attr('data-ordering') == "false" ? false : true) : true;
    let searching = datatable.attr('data-paginate') ? (that.attr('data-searching') == "false" ? false : true) : true;


    let params = {
        "oLanguage": dataTableLangageParams,
        "bLengthChange": bLengthChange,
        "bPaginate": bPaginate,
        "responsive": {
            "details": {
                "target": -1
            }
        },
        "ordering": ordering,
        "searching": searching
    };

    if (datatable.attr('data-ajax')) {
        params.ajax = datatable.attr('data-ajax');
    }

    datatable.DataTable(params);
};

app.initDataTables = function () {
    $('.datatable').each(function () {
        app.initOneDataTable($(this));
    });

};

app.initBackUserPageBtn = function () {
    $('.btn-user-edit').each(function () {
        $(this).on('click', function (e) {
            app.createModal('Modifier un utilisateur', $(this).attr('edit-url'));
        });
    });
    $('.btn-user-delete').each(function () {
        $(this).on('click', function (e) {
            $.ajax({
                url: $(this).attr('delete-url'),
                method: 'GET',
                success: function (data, textStatus, xhr) {
                    app.reloadDatatables(); //TODO
                },
                error: function (xhr, textStatus, codeError) {
                    app.addFlash('error', 'Erreur de chargement');
                }
            });
        });
    });
};

/**
 * Create Modal
 *
 * @param {string} title
 * @param {string} dataUrl
 * @param isForm
 * @param childModal If open a modal in a modal set it to true
 */
app.createModal = function (title, dataUrl, isForm = true, childModal = false) {
    let $modalBody = $('.modal .modal-body');
    let spinnerUrl = $modalBody.attr('spinner-url');

    //Spinner
    $modalBody.html('<img class="spinner-modal" src="' + spinnerUrl + '"/>');

    if (title) {
        $('.modal .modal-title').html(title);
        $('.modal .modal-header').removeClass('hidden');
    } else {
        $('.modal .modal-header').addClass('hidden');
    }

    $.ajax({
        url: dataUrl,
        method: 'GET',
        success: function (data, textStatus, xhr) {
            $modalBody.html(data);
            app.initApp(true);
            let $btnValidate = $('.btn-validate-modal');
            // TODO AjaxResponse
            if (isForm) {
                $btnValidate.unbind('click');
                $btnValidate.on('click', function (e) {
                    e.preventDefault();
                    let $form = $('.modal-body form');
                    var url = $form.attr('action');
                    $modalBody.html('<img class="spinner-modal" src="' + spinnerUrl + '"/>');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $form.serialize(),
                        success: function (data, textStatus, codeError) {
                            $modalBody.html(data);
                            app.ajaxHandleCallbacks(data);
                        },
                        error: function (xhr, textStatus, codeError) {
                            app.addFlash('error', 'Erreur de chargement');
                        }
                    });
                });
            } else {
                $('.btn-validate-modal').hide();
                $('.btn-close-modal').on('click', function () {
                    app.reloadDatatables();
                });
            }
        },
        error: function (xhr, textStatus, codeError) {
            app.addFlash('error', 'Erreur de chargement');
        }
    });

    if (!childModal) {
        $('.btn-show-favorite-modal').click();
    }
};

/**
 *
 * @param json
 */
app.ajaxHandleCallbacks = function (json) {
    if (typeof json === 'object') {
        // Callbacks à exécuter
        if (json.data.hasOwnProperty('callbacks') && Array.isArray(json.data.callbacks)) {
            for (var i = 0; i < json.data.callbacks.length; i++) {
                if (json.data.hasOwnProperty('arguments') && Array.isArray(json.data.arguments)) {
                    window.app[json.data.callbacks[i]](json.data.arguments[i]);
                }
            }
        }

        // Messages flash
        if (json.data.hasOwnProperty('flash_type') && json.data.hasOwnProperty('flash_message')) {
            app.addFlash(json.data.flash_type, json.data.flash_message);
        }
    }
};

/**
 * Add error flash message
 *
 * @param {string} message
 */
app.addFlashError = function (message) {
    app.addFlash('error', message);
};

/**
 * Add flash message
 *
 * @param {string} type
 * @param {string} message
 */
app.addFlash = function (type, message) {
    $('#flash_' + type + ' .flash-message').html(message);
    $('.flash-messages').css('display', 'block').find('#flash_' + type).removeClass('hidden').delay(1000).fadeIn(600).delay(6000).fadeOut(600);

    if (type == 'success') {
        $('#flash_error').css('display', 'none');
    } else if (type == 'error') {
        $('#flash_success').css('display', 'none');
    }
};

/**
 * Close modal
 */
app.closeModal = function () {
    $('.btn-close-modal').click();
};

app.reloadDatatables = function () {
    $('.datatable').each(function () {
        $(this).DataTable().destroy();
        app.initOneDataTable($(this));
    });
};

app.initAjaxReload = function (id) {
    if (id === undefined || id === null) {
        $('.ajax-reload').each(function () {
            app.handleAjaxReload($(this))
        });
    } else {
        app.handleAjaxReload($(id));
    }
};

app.handleAjaxReload = function ($bloc) {
    let spinnerUrl = $bloc.attr('spinner-url');
    //Spinner
    $bloc.html('<img class="spinner-modal" src="' + spinnerUrl + '"/>');

    let url = $bloc.attr('ajax-url');
    $.ajax({
        url: url,
        method: 'GET',
        success: function (data, textStatus, xhr) {
            $bloc.html(data);
        },
        error: function (xhr, textStatus, codeError) {
            app.addFlash('error', 'Erreur de chargement');
        }
    });
};

/**
 * Init the favorite select
 */
app.initFavorite = function () {
    let $selectFavorite = $('.select-favorite');
    let $spanSpinner = $('.favorite-spinner');
    $selectFavorite.on('change', (e) => {
        if ($selectFavorite.val() !== '' && $selectFavorite.val() !== 'undefined' && $selectFavorite.val() !== undefined) {
            let spinnerUrl = $spanSpinner.attr('spinner-url');
            //Spinner
            $spanSpinner.html('<img class="spinner-modal" src="' + spinnerUrl + '"/>');

            $.ajax({
                type: "GET",
                url: $('.div-favorite').attr('data-url') + '/' + $selectFavorite.val(),
                success: function (data) {
                    $('.select-start').val(data.start).trigger('change');
                    $('.select-end').val(data.stop).trigger('change');
                    $spanSpinner.html('');
                },
                error: function (xhr, textStatus, codeError) {
                    app.addFlash('error', 'Erreur de chargement');
                }
            });
        }
    });
};
// TODO : Refactoriser
app.initLoginForm = function () {
    $('.btn-login').on('click', function (e) {
        e.preventDefault();
        app.createModal('Gestion de compte', $(this).attr('href'), true);
    });
};

app.initForgotPasswordForm = function () {
    $('.btn-forgot').on('click', function (e) {
        e.preventDefault();
        app.createModal('J\'ai oublié mon mot de passe', $(this).attr('href'), true, true);
    });
};

app.initAddFavoriteForm = function () {
    $('.btn-add-favorite').on('click', function (e) {
        e.preventDefault();
        app.createModal('Ajouter des favoris', $(this).attr('href'), true);
    });
};

app.reloadPage = function () {
    location.reload();
};