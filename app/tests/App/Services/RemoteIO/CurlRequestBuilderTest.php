<?php

namespace App\Tests\App\Services\RemoteIO;

use App\Entity\Stops\T2CStop;
use App\Model\T2CRoute\T2CRouteSearch;
use App\Services\RemoteIO\CurlRequestBuilder;
use App\Services\RemoteIO\CurlRequestQuery;
use PHPUnit\Framework\TestCase;

class CurlRequestBuilderTest extends TestCase
{
    /**
     * @var T2CRouteSearch
     */
    private $t2cRouteSearch;

    /**
     * @var CurlRequestBuilder
     */
    private $curlRequestBuilder;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $t2cStopStart = (new T2CStop())->setName('Début');
        $t2cStopEnd = (new T2CStop())->setName('Fin');
        $this->t2cRouteSearch = (new T2CRouteSearch())->setStopStart($t2cStopStart)->setStopEnd($t2cStopEnd);
        $this->curlRequestBuilder = new CurlRequestBuilder();
    }

    /**
     * @throws \Exception
     */
    public function testFindRoutesFromNow()
    {
        $result = $this->curlRequestBuilder->findRoutesFromNow($this->t2cRouteSearch);
        $this->assertInternalType('array', $result);
        $this->assertEquals('Début', $result['data']['dpt']);
        $this->assertEquals('Fin', $result['data']['apt']);
        $this->assertEquals($result['data']['dy'], (new \DateTime())->format('Y-m-d'));
        $this->assertEquals(CurlRequestQuery::METHOD_GET, $result['method']);
    }
}
