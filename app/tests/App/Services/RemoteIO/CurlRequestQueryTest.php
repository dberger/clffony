<?php

namespace App\Tests\App\Services\RemoteIO;

use App\Services\RemoteIO\CurlRequestQuery;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class CurlRequestQueryTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $request;

    /**
     * @var string
     */
    private $t2cUrl;

    /**
     * @var CurlRequestQuery
     */
    private $curlRequestQuery;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->request = $this->prophesize(RequestStack::class);
        $this->t2cUrl = 'http://t2c-prod.rcsmobility.com/synthese?SERVICE=page&p=17733060971724812&pi=0';
        $this->curlRequestQuery = new CurlRequestQuery($this->request->reveal(), $this->t2cUrl);
    }

    /**
     * @throws \Exception
     */
    public function testQueryGet()
    {
        $request = new Request([], [], [], [], [], [], json_encode([
            'foo' => 'bar'
        ]));

        $requestStack = new RequestStack();
        $requestStack->push($request);
        $params = [
            'method' => CurlRequestQuery::METHOD_GET,
            'data' => [
                'dpt' => 'CHU',
                'apt' => 'Jaude',
                'dy' => (new \DateTime())->format('Y-m-d')
            ]
        ];

        $this->request->getCurrentRequest()->shouldBeCalled()->willReturn($request);
        $result = $this->curlRequestQuery->query($params);

        $this->assertInternalType('array', $result);
    }

    /**
     * @throws \Exception
     */
    public function testQueryPost()
    {
        $request = new Request([], [], [], [], [], [], json_encode([
            'foo' => 'bar'
        ]));

        $requestStack = new RequestStack();
        $requestStack->push($request);
        $params = [
            'method' => CurlRequestQuery::METHOD_POST,
            'data' => [
                'dpt' => 'CHU',
                'apt' => 'Jaude',
                'dy' => (new \DateTime())->format('Y-m-d')
            ]
        ];

        $this->request->getCurrentRequest()->shouldBeCalled()->willReturn($request);
        $result = $this->curlRequestQuery->query($params);

        $this->assertNull($result);
    }

    /**
     * @throws \Exception
     */
    public function testQueryWithoutParams()
    {
        $result = $this->curlRequestQuery->query();

        $this->assertNull($result);
    }
}
