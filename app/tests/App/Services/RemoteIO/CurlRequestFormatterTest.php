<?php

namespace App\Tests\App\Services\RemoteIO;

use App\Entity\Route\T2CRoute;
use App\Entity\Stops\T2CStop;
use App\Model\T2CRoute\T2CRouteSearch;
use App\Services\RemoteIO\CurlRequestFormatter;
use PHPUnit\Framework\TestCase;

class CurlRequestFormatterTest extends TestCase
{
    /**
     * @var T2CRouteSearch
     */
    private $t2cRouteSearch;

    /**
     * @var CurlRequestFormatter
     */
    private $curlRequestFormatter;

    /**
     * @var array
     */
    private $fakedQueryResults;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $t2cStopStart = (new T2CStop())->setName('Début');
        $t2cStopEnd = (new T2CStop())->setName('Fin');
        $this->t2cRouteSearch = (new T2CRouteSearch())->setStopStart($t2cStopStart)->setStopEnd($t2cStopEnd)->setMaxResults(63);
        $this->curlRequestFormatter = new CurlRequestFormatter();
        $this->fakedQueryResults = [
            'board' => [
                0 => [
                    '@attributes' => [
                        'dt' => '21:13',
                        'at' => '21:21'
                    ]
                ]
            ]
        ];
    }

    /**
     * @throws \Exception
     */
    public function testConvertResultsToRouteObject()
    {
        /** @var T2CRoute[] $result */
        $result = $this->curlRequestFormatter->convertResultsToRouteObject($this->fakedQueryResults, $this->t2cRouteSearch);
        $this->assertInternalType('array', $result);
        $this->assertEquals($this->t2cRouteSearch->getStopStart(), $result[0]->getStopStart());
        $this->assertEquals($this->t2cRouteSearch->getStopEnd(), $result[0]->getStopEnd());
        $this->assertEquals(\DateTime::createFromFormat('H:i', $this->fakedQueryResults['board'][0]['@attributes']['dt']), $result[0]->getHourStart());
        $this->assertEquals(\DateTime::createFromFormat('H:i', $this->fakedQueryResults['board'][0]['@attributes']['at']), $result[0]->getHourEnd());
    }

    /**
     * @throws \Exception
     */
    public function testConvertResultsToRouteObjectWithNoResult()
    {
        $result = $this->curlRequestFormatter->convertResultsToRouteObject([], $this->t2cRouteSearch);
        $this->assertNull($result);
    }
}
