<?php

namespace App\Tests\App\Services\Import;

use App\Entity\Lines\T2CLine;
use App\Entity\Stops\T2CStop;
use App\Services\Import\T2C\T2CImporter;
use App\Services\Import\T2C\T2CUpdater;
use App\Services\T2CLine\T2CLineSearchProvider;
use App\Services\T2CStop\T2CStopCrudManager;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * Class T2CUpdaterTest
 * @package App\Tests\App\Services\Import
 */
class T2CUpdaterTest extends TestCase
{

    /**
     * @var ObjectProphecy
     */
    private $t2cStopsImporter;

    /**
     * @var ObjectProphecy
     */
    private $t2cLineSearchProvider;

    /**
     * @var ObjectProphecy
     */
    private $t2cStopCrudManager;

    /**
     * @var T2CUpdater
     */
    private $t2cUpdater;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->t2cStopsImporter = $this->prophesize(T2CImporter::class);
        $this->t2cLineSearchProvider = $this->prophesize(T2CLineSearchProvider::class);
        $this->t2cStopCrudManager = $this->prophesize(T2CStopCrudManager::class);
        $this->t2cUpdater = new T2CUpdater($this->t2cStopsImporter->reveal(), $this->t2cLineSearchProvider->reveal(), $this->t2cStopCrudManager->reveal());
    }

    public function testUpdateDatabase()
    {
        $lineALocal = ((new T2CLine())->setName('Ligne A'))->addStop((new T2CStop())->setName('CHU'));
        $lineARemote = ((new T2CLine())->setName('Ligne A'))->addStop((new T2CStop())->setName('Jaude'));

        $localLines = [$lineALocal];
        $remoteLines = [$lineARemote];

        $this->t2cStopsImporter->import()->shouldBeCalled()->willReturn($remoteLines);

        $this->t2cLineSearchProvider->findAll()->shouldBeCalled()->willReturn($localLines);

        $result = $this->t2cUpdater->updateDatabase();

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(T2CStop::class, $result[0]);
        $this->assertEquals('Jaude', $result[0]->getName());
    }
}
