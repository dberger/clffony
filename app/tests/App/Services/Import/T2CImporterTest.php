<?php

namespace App\Tests\App\Services\Import;

use App\Entity\Lines\T2CLine;
use App\Entity\Stops\T2CStop;
use App\Services\Import\Base\BaseLinesImporter;
use App\Services\Import\Base\BaseStopsImporter;
use App\Services\Import\T2C\T2CImporter;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class T2CImporterTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $baseLineImporter;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $baseStopImporter;

    /**
     * @var T2CImporter
     */
    private $t2cImporter;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->baseLineImporter = $this->prophesize(BaseLinesImporter::class);
        $this->baseStopImporter = $this->prophesize(BaseStopsImporter::class);
        $this->t2cImporter = new T2CImporter($this->baseLineImporter->reveal(), $this->baseStopImporter->reveal());
    }

    /**
     * @throws \Exception
     */
    public function testImport()
    {
        $line = (new T2CLine())->setName('Ligne A');

        $this->baseLineImporter->importLines()->shouldBeCalled()->willReturn(new ArrayCollection([$line]));
        $this->baseStopImporter->importStops($line)->shouldBeCalled()->willReturn(new ArrayCollection([new T2CStop()]));

        $result = $this->t2cImporter->import();

        $this->assertInstanceOf(ArrayCollection::class, $result);
    }
}
