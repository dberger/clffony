<?php

namespace App\Tests\App\Services\Import;

use App\Services\Import\T2C\T2CLinesImporter;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class T2CLinesImporterTest extends TestCase
{

    /**
     * @var T2CLinesImporter
     */
    private $t2cLinesImporter;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->t2cLinesImporter = new T2CLinesImporter();
    }

    public function testImportLines()
    {
        $result = $this->t2cLinesImporter->importLines();

        $this->assertInstanceOf(ArrayCollection::class, $result);
    }
}
