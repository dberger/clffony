<?php

namespace App\Tests\App\Services\Import;

use App\Entity\Lines\T2CLine;
use App\Services\Import\T2C\T2CStopsImporter;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class T2CStopsImporterTest extends TestCase
{

    /**
     * @var T2CStopsImporter
     */
    private $t2cStopsImporter;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->t2cStopsImporter = new T2CStopsImporter();
    }

    public function testImportLines()
    {
        $line = (new T2CLine())->setName('Ligne A');
        $result = $this->t2cStopsImporter->importStops($line);

        $this->assertInstanceOf(ArrayCollection::class, $result);
    }
}
