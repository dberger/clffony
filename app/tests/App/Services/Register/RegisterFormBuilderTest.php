<?php


namespace App\Tests\App\Services\Register;

use App\Form\User\RegisterType;
use App\Services\Register\RegisterFormBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class RegisterFormBuilderTest extends TestCase
{
    /**
     * @var RegisterFormBuilder
     */
    private $registerFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->registerFormBuilder = new RegisterFormBuilder($this->formFactory->reveal());
    }

    public function testGetForm()
    {
        $url = '#';

        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->createNamed('', RegisterType::class, null, ['action' => $url])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->registerFormBuilder->getRegisterForm($url);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
