<?php


namespace App\Tests\App\Services\Register;

use App\Entity\User\User;
use App\Services\Register\RegisterFormHandler;
use Doctrine\ORM\EntityManager;
use MsgPhp\User\Command\CreateUserCommand;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\TraceableMessageBus;

class RegisterFormHandlerTest extends TestCase
{
    /**
     * @var RegisterFormHandler
     */
    private $registerFormHandler;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $request;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $bus;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $em;

    protected function setUp()
    {
        $this->request = $this->prophesize(RequestStack::class);
        $this->bus = $this->prophesize(TraceableMessageBus::class);
        $this->em = $this->prophesize(EntityManager::class);
        $this->registerFormHandler = new RegisterFormHandler($this->request->reveal(), $this->bus->reveal(), $this->em->reveal());
    }

    public function testHandleValid()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $data = ['email' => 'clffony@clffony.fr'];

        $form->handleRequest($request)->shouldBeCalled();
        $form->getData()->shouldBeCalled()->willReturn($data);
        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);

        $msg = new CreateUserCommand($data);
        $this->bus->dispatch($msg)->shouldBeCalled()->willReturn(new Envelope(new User(new UserId(1), 'clffony@clffony.fr', 'test')));

        $result = $this->registerFormHandler->handle($form->reveal(), $request->reveal());
        $this->assertTrue($result);
    }

    public function testHandleNotValidNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->registerFormHandler->handle($form->reveal(), $request->reveal());

        $this->assertNull($result);
    }
}
