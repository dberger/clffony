<?php


namespace App\Tests\App\Services\Login;

use App\Form\User\LoginType;
use App\Services\Login\LoginFormBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginFormBuilderTest extends TestCase
{
    /**
     * @var LoginFormBuilder
     */
    private $loginFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $authUtils;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->authUtils = $this->prophesize(AuthenticationUtils::class);
        $this->loginFormBuilder = new LoginFormBuilder($this->formFactory->reveal(), $this->authUtils->reveal());
    }

    public function testGetForm()
    {
        $url = '#';
        $lastUsername = 'test@test.fr';

        $this->authUtils->getLastUsername()->shouldBeCalled()->willReturn($lastUsername);

        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->createNamed('', LoginType::class, ['email' => $lastUsername], ['action' => $url])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->loginFormBuilder->getLoginForm($url);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
