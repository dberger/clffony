<?php

namespace App\Tests\App\Services\T2CStop;

use App\Entity\Lines\T2CLine;
use App\Entity\Stops\T2CStop;
use App\Services\T2CStop\T2CStopDataTableLoader;
use App\Services\T2CStop\T2CStopSearchProvider;
use PHPUnit\Framework\TestCase;

class MedicalControlDataTableLoaderTest extends TestCase
{

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $t2cStopSearchProvider;

    /**
     * @var T2CStopDataTableLoader
     */
    private $t2cStopDataTableLoader;

    protected function setUp()
    {
        $this->t2cStopSearchProvider = $this->prophesize(T2CStopSearchProvider::class);
        $this->t2cStopDataTableLoader = new T2CStopDataTableLoader($this->t2cStopSearchProvider->reveal());
    }

    public function testFormatStopsDataForDataTable()
    {
        $stop = (new T2CStop())->setName('Tests')->setLine((new T2CLine())->setName('phpunit'))->setId(63);
        $data = [$stop];

        $this->t2cStopSearchProvider->findAll()->shouldBeCalled()->willReturn($data);

        $result = $this->t2cStopDataTableLoader->getAllStops();

        $this->assertInternalType('array', $result);
        $this->assertEquals($result['data'][0][0], '63');
        $this->assertEquals($result['data'][0][1], 'Tests');
        $this->assertEquals($result['data'][0][2], 'phpunit');
    }
}
