<?php

namespace App\Tests\App\Services\T2CStop;

use App\Entity\Stops\T2CStop;
use App\Services\T2CStop\T2CStopCrudManager;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class T2CStopCrudManagerTest extends TestCase
{
    /**
     * @var T2CStopCrudManager
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $entityManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $eventDispatcher;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $csrfTokenManager;

    protected function setUp()
    {
        $this->entityManager = $this->prophesize(EntityManager::class);
        $this->eventDispatcher = $this->prophesize(EventDispatcher::class);
        $this->csrfTokenManager = $this->prophesize(CsrfTokenManager::class);
        $this->crudManager = new T2CStopCrudManager($this->entityManager->reveal(), $this->eventDispatcher->reveal(), $this->csrfTokenManager->reveal());
    }

    public function testSave()
    {
        $entity = new T2CStop();
        $entity->setName('SuperStop');

        $this->entityManager->persist($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();

        $result = $this->crudManager->save($entity);

        $this->assertInstanceOf(T2CStop::class, $result);
    }

    public function testDeleteTrue()
    {
        $token = new CsrfToken("delete_t2cStop", "goodToken");
        $entity = new T2CStop();
        $entity->setName('SuperStop');

        $this->csrfTokenManager->getToken('delete_t2cStop')->shouldBeCalled()->willReturn($token);
        $this->entityManager->remove($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();
        $this->csrfTokenManager->removeToken('delete_t2cStop')->shouldBeCalled();

        $result = $this->crudManager->delete($entity, "goodToken");

        $this->assertTrue($result);
    }

    public function testDeleteFalse()
    {
        $token = new CsrfToken("delete_t2cStop", "goodToken");
        $entity = new T2CStop();
        $entity->setName('Super Stop');

        $this->csrfTokenManager->getToken('delete_t2cStop')->shouldBeCalled()->willReturn($token);

        $result = $this->crudManager->delete($entity, "anotherToken");

        $this->assertFalse($result);
    }
}
