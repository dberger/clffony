<?php
namespace App\Tests\App\Services\T2CLine;

use App\Entity\Stops\T2CStop;
use App\Repository\T2CStopRepository;
use App\Services\T2CStop\T2CStopSearchProvider;
use PHPUnit\Framework\TestCase;

class T2CStopSearchProviderTest extends TestCase
{
    /**
     * @var T2CStopSearchProvider
     */
    private $searchProvider;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $repository;

    protected function setUp()
    {
        $this->repository = $this->prophesize(T2CStopRepository::class);
        $this->searchProvider = new T2CStopSearchProvider($this->repository->reveal());
    }

    public function testFind()
    {
        $expected = new T2CStop();
        $id = 1;

        $this->repository->find($id)->shouldBeCalled()->willReturn($expected);

        $result = $this->searchProvider->find($id);

        $this->assertEquals($expected, $result);
    }

    public function testFindAll()
    {
        $arrayCollection = [];
        $this->repository->findAll()->shouldBeCalled()->willReturn($arrayCollection);
        $arrayCollection = $this->searchProvider->findAll();
        $this->assertInternalType('array', $arrayCollection);
    }
}
