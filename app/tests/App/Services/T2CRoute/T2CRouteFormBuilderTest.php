<?php

namespace App\Tests\App\Services\T2CRoute;

use App\Entity\User\User;
use App\Form\T2CRouteSearchType;
use App\Model\T2CRoute\T2CRouteSearch;
use App\Services\T2CRoute\T2CRouteFormBuilder;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class T2CRouteFormBuilderTest extends TestCase
{
    /**
     * @var T2CRouteFormBuilder
     */
    private $t2cFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->t2cFormBuilder = new T2CRouteFormBuilder($this->formFactory->reveal());
    }

    public function testGetFormWithT2CRouteSearch()
    {
        $url = '#';

        $user = new User(new UserId(1), 'd-berger@sfi.fr', 'locked');
        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->create(T2CRouteSearchType::class, new T2CRouteSearch(), ['action' => $url, 'user' => $user, 'csrf_protection' => true])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->t2cFormBuilder->getForm($url, $user);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
