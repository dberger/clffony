<?php

namespace App\Tests\App\Services\T2CRoute;

use App\Entity\Route\T2CRoute;
use App\Services\T2CFavorite\T2CFavoriteCrudManager;
use App\Services\T2CRoute\T2CRouteFormHandler;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class T2CRouteFormHandlerTest extends TestCase
{
    /**
     * @var T2CRouteFormHandler
     */
    private $formHandler;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $requestStack;

    protected function setUp()
    {
        $this->requestStack = $this->prophesize(RequestStack::class);
        $this->crudManager = $this->prophesize(T2CFavoriteCrudManager::class);
        $this->formHandler = new T2CRouteFormHandler($this->requestStack->reveal());
    }

    public function testHandleWithFormSubmittedAndValid()
    {
        $entity = new T2CRoute();

        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);
        $form->getData()->shouldBeCalled()->willReturn($entity);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal());

        $this->assertEquals($result, $result);
    }

    public function testHandleWithFormNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal());

        $this->assertInternalType('array', $result);
    }

    public function testHandleWithFormSubmittedNotValid()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal());

        $this->assertNull($result);
    }
}
