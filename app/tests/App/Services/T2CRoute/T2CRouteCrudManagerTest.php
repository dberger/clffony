<?php

namespace App\Tests\App\Services\T2CRoute;

use App\Entity\Route\T2CRoute;
use App\Services\T2CRoute\T2CRouteCrudManager;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class T2CRouteCrudManagerTest extends TestCase
{
    /**
     * @var T2CRouteCrudManager
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $entityManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $eventDispatcher;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $csrfTokenManager;

    protected function setUp()
    {
        $this->entityManager = $this->prophesize(EntityManager::class);
        $this->eventDispatcher = $this->prophesize(EventDispatcher::class);
        $this->csrfTokenManager = $this->prophesize(CsrfTokenManager::class);
        $this->crudManager = new T2CRouteCrudManager($this->entityManager->reveal(), $this->eventDispatcher->reveal(), $this->csrfTokenManager->reveal());
    }

    public function testSave()
    {
        $entity = new T2CRoute();

        $this->entityManager->persist($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();

        $result = $this->crudManager->save($entity);

        $this->assertInstanceOf(T2CRoute::class, $result);
    }

    public function testDeleteTrue()
    {
        $token = new CsrfToken("delete_t2cRoute", "goodToken");
        $entity = new T2CRoute();

        $this->csrfTokenManager->getToken('delete_t2cRoute')->shouldBeCalled()->willReturn($token);
        $this->entityManager->remove($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();
        $this->csrfTokenManager->removeToken('delete_t2cRoute')->shouldBeCalled();

        $result = $this->crudManager->delete($entity, "goodToken");

        $this->assertTrue($result);
    }

    public function testDeleteFalse()
    {
        $token = new CsrfToken("delete_t2cRoute", "goodToken");
        $entity = new T2CRoute();

        $this->csrfTokenManager->getToken('delete_t2cRoute')->shouldBeCalled()->willReturn($token);

        $result = $this->crudManager->delete($entity, "anotherToken");

        $this->assertFalse($result);
    }
}
