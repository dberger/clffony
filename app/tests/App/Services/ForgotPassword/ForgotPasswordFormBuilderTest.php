<?php


namespace App\Tests\App\Services\ForgotPassword;

use App\Form\User\ForgotPasswordType;
use App\Services\ForgotPassword\ForgotPasswordFormBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class ForgotPasswordFormBuilderTest extends TestCase
{
    /**
     * @var ForgotPasswordFormBuilder
     */
    private $forgotPasswordFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->forgotPasswordFormBuilder = new ForgotPasswordFormBuilder($this->formFactory->reveal());
    }

    public function testGetForm()
    {
        $url = '#';

        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->createNamed('', ForgotPasswordType::class, null, ['action' => $url])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->forgotPasswordFormBuilder->getForgotPasswordForm($url);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
