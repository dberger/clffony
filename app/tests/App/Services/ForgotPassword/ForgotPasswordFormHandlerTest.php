<?php


namespace App\Tests\App\Services\ForgotPassword;

use App\Entity\User\User;
use App\Repository\UserRepository;
use App\Services\ForgotPassword\ForgotPasswordFormHandler;
use Doctrine\ORM\EntityManager;
use MsgPhp\User\Command\RequestUserPasswordCommand;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\TraceableMessageBus;

class ForgotPasswordFormHandlerTest extends TestCase
{
    /**
     * @var ForgotPasswordFormHandler
     */
    private $forgotPasswordFormHandler;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $request;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $bus;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $em;

    protected function setUp()
    {
        $this->request = $this->prophesize(RequestStack::class);
        $this->bus = $this->prophesize(TraceableMessageBus::class);
        $this->em = $this->prophesize(EntityManager::class);
        $this->forgotPasswordFormHandler = new ForgotPasswordFormHandler($this->request->reveal(), $this->bus->reveal(), $this->em->reveal());
    }

    public function testHandle()
    {
        $form = $this->prophesize(Form::class);
        $user = new User(new UserId(1), 'clffony@clffony.fr', 'test');
        $request = $this->prophesize(Request::class);
        $data = ['email' => 'clffony@clffony.fr'];
        $userRepo = $this->prophesize(UserRepository::class);

        $form->handleRequest($request)->shouldBeCalled();
        $form->getData()->shouldBeCalled()->willReturn($data);
        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);

        $msg = new RequestUserPasswordCommand($user->getId());
        $this->bus->dispatch($msg)->shouldBeCalled()->willReturn(new Envelope($user));

        $this->em->getRepository(User::class)->shouldBeCalled()->willReturn($userRepo);
        $userRepo->findOneBy(['credential.email' => 'clffony@clffony.fr'])->shouldBeCalled()->willReturn($user);

        $result = $this->forgotPasswordFormHandler->handle($form->reveal(), $request->reveal());

        $this->assertInstanceOf(User::class, $result);
    }

    public function testHandleValidNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->forgotPasswordFormHandler->handle($form->reveal(), $request->reveal());

        $this->assertNull($result);
    }
}
