<?php

namespace App\Tests\App\Services\T2CStop;

use App\Entity\User\User;
use App\Services\User\UserDataTableLoader;
use App\Services\User\UserSearchProvider;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

class UserDataTableLoaderTest extends TestCase
{

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $userSearchProvider;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $twig;

    /**
     * @var UserDataTableLoader
     */
    private $userDataTableLoader;

    protected function setUp()
    {
        $this->userSearchProvider = $this->prophesize(UserSearchProvider::class);
        $this->twig = $this->prophesize(Environment::class);
        $this->userDataTableLoader = new UserDataTableLoader($this->userSearchProvider->reveal(), $this->twig->reveal());
    }

    public function testGetAllUsers()
    {
        $user = new User(new UserId(1), 'clffony@clffony.fr', '');
        $data = [$user];

        $this->userSearchProvider->findAll()->shouldBeCalled()->willReturn($data);

        $result = $this->userDataTableLoader->getAllUsers();

        $this->assertInternalType('array', $result);
        $this->assertEquals($result['data'][0][1], 'clffony@clffony.fr');
        $this->assertEquals($result['data'][0][2], 'ROLE_USER');
        $this->assertEquals($result['data'][0][3], null);
    }
}
