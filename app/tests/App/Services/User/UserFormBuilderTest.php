<?php

namespace App\Tests\App\Services\T2CRoute;

use App\Entity\User\User;
use App\Form\User\UserEditType;
use App\Form\User\UserType;
use App\Services\User\UserFormBuilder;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class UserFormBuilderTest extends TestCase
{
    /**
     * @var UserFormBuilder
     */
    private $userFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->userFormBuilder = new UserFormBuilder($this->formFactory->reveal());
    }

    public function testGetEditFormWithUser()
    {
        $url = '#';

        $user = new User(new UserId(1), 'd-berger@sfi.fr', 'locked');
        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->create(UserEditType::class, null, ['action' => $url, 'user' => $user])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->userFormBuilder->getEditForm($url, $user);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }

    public function testGetFormWithUser()
    {
        $url = '#';

        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->create(UserType::class, null, ['action' => $url])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->userFormBuilder->getForm($url);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
