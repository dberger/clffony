<?php
namespace App\Tests\App\Services\User;

use App\Entity\User\User;
use App\Model\User\UserSearch;
use App\Repository\UserRepository;
use App\Services\User\UserSearchProvider;
use Doctrine\DBAL\Query\QueryBuilder;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;

class UserSearchProviderTest extends TestCase
{
    /**
     * @var UserSearchProvider
     */
    private $searchProvider;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $repository;

    protected function setUp()
    {
        $this->repository = $this->prophesize(UserRepository::class);
        $this->searchProvider = new UserSearchProvider($this->repository->reveal());
    }

    public function testFind()
    {
        $expected = new User(new UserId(1), 'clffony@clffony.fr', '');
        $id = 1;

        $this->repository->find($id)->shouldBeCalled()->willReturn($expected);

        $result = $this->searchProvider->find($id);

        $this->assertEquals($expected, $result);
    }

    public function testFindAll()
    {
        $arrayCollection = [];
        $this->repository->findAll()->shouldBeCalled()->willReturn($arrayCollection);
        $arrayCollection = $this->searchProvider->findAll();
        $this->assertInternalType('array', $arrayCollection);
    }

    public function testSearch()
    {
        $search = (new UserSearch());

        $arrayCollection = [];
        $queryBuilder = $this->prophesize(QueryBuilder::class);
        $this->repository->getSearchQuery($search)->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->execute()->shouldBeCalled()->willReturn($arrayCollection);

        $arrayCollection = $this->searchProvider->search($search);
        $this->assertInternalType('array', $arrayCollection);
    }
}
