<?php

namespace App\Tests\App\Services\User;

use App\Entity\Stops\T2CStop;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Services\User\UserCrudManager;
use App\Services\User\UserFormHandler;
use MsgPhp\Domain\Factory\EntityAwareFactoryInterface;
use MsgPhp\User\Command\ChangeUserCredentialCommand;
use MsgPhp\User\Command\CreateUserCommand;
use MsgPhp\User\Repository\UserRepositoryInterface;
use MsgPhp\User\Repository\UserRoleRepositoryInterface;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\TraceableMessageBus;

class T2CFavoriteFormHandlerTest extends TestCase
{
    /**
     * @var UserFormHandler
     */
    private $formHandler;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $bus;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $userRoleRepository;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $userRepository;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $factory;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $requestStack;

    /**
     * @var User
     */
    private $user;


    protected function setUp()
    {
        $this->requestStack = $this->prophesize(RequestStack::class);
        $this->crudManager = $this->prophesize(UserCrudManager::class);
        $this->bus = $this->prophesize(TraceableMessageBus::class);
        $this->userRoleRepository = $this->prophesize(UserRoleRepositoryInterface::class);
        $this->userRepository = $this->prophesize(UserRepositoryInterface::class);
        $this->formHandler = new UserFormHandler(
            $this->requestStack->reveal(),
            $this->bus->reveal(),
            $this->crudManager->reveal(),
            $this->userRoleRepository->reveal(),
            $this->userRepository->reveal()
        );
        $this->user = new User(new UserId(1), 'test@clffony.fr', 'tests');
    }

    public function testHandleWithFormSubmittedAndValid()
    {
        $user = new User(new UserId(1), 'clffony@clffony.fr', '');
        $role = new Role('ROLE_TEST');
        $data = ['email' => 'clffony@clffony.fr'];
        $form = $this->prophesize(Form::class);
        $formRole = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);
        $form->get('register')->shouldBeCalled()->willReturn($form);
        $form->getData()->shouldBeCalled()->willReturn($data);
        $msg = new CreateUserCommand($data);
        $this->bus->dispatch($msg)->shouldBeCalled()->willReturn(new Envelope(new T2CStop()));

        $form->get('role')->shouldBeCalled()->willReturn($formRole);
        $formRole->getData()->shouldBeCalled()->willReturn($role);

        $this->userRepository->findByUsername($data['email'])->shouldBeCalled()->willReturn($user);
        $this->userRoleRepository->save(Argument::type(\App\Entity\User\UserRole::class))->shouldBeCalled();
        $result = $this->formHandler->handle($form->reveal(), $request->reveal(), $this->user);

        $this->assertEquals($result, $result);
    }

    public function testHandleWithFormNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal(), $this->user);

        $this->assertInternalType('array', $result);
    }

    public function testHandleWithFormSubmittedNotValid()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal(), $this->user);

        $this->assertNull($result);
    }

    public function testHandleEditWithFormSubmittedAndValid()
    {
        $data = ['password' => 'superMdp'];
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);
        $form->get('resetPassword')->shouldBeCalled()->willReturn($form);
        $form->getData()->shouldBeCalled()->willReturn($data);
        $msg = new ChangeUserCredentialCommand($this->user->getId(), $data);
        $this->bus->dispatch($msg)->shouldBeCalled()->willReturn(new Envelope(new T2CStop()));

        $result = $this->formHandler->handleEdit($form->reveal(), $request->reveal(), $this->user);

        $this->assertEquals($result, $result);
    }

    public function testHandleEditWithFormNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handleEdit($form->reveal(), $request->reveal(), $this->user);

        $this->assertInternalType('array', $result);
    }

    public function testHandleEditWithFormSubmittedNotValid()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handleEdit($form->reveal(), $request->reveal(), $this->user);

        $this->assertNull($result);
    }
}
