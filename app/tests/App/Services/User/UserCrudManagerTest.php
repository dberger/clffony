<?php

namespace App\Tests\App\Services\User;

use App\Entity\User\User;
use App\Services\User\UserCrudManager;
use Doctrine\ORM\EntityManager;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class UserCrudManagerTest extends TestCase
{
    /**
     * @var UserCrudManager
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $entityManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $eventDispatcher;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $csrfTokenManager;

    protected function setUp()
    {
        $this->entityManager = $this->prophesize(EntityManager::class);
        $this->eventDispatcher = $this->prophesize(EventDispatcher::class);
        $this->csrfTokenManager = $this->prophesize(CsrfTokenManager::class);
        $this->crudManager = new UserCrudManager($this->entityManager->reveal(), $this->eventDispatcher->reveal(), $this->csrfTokenManager->reveal());
    }

    public function testSave()
    {
        $entity = new User(new UserId(1), '', '');

        $this->entityManager->persist($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();

        $result = $this->crudManager->save($entity);

        $this->assertInstanceOf(User::class, $result);
    }

    public function testDeleteTrue()
    {
        $token = new CsrfToken("delete_user", "goodToken");
        $entity = new User(new UserId(1), '', '');

        $this->csrfTokenManager->getToken('delete_user')->shouldBeCalled()->willReturn($token);
        $this->entityManager->remove($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();
        $this->csrfTokenManager->removeToken('delete_user')->shouldBeCalled();

        $result = $this->crudManager->delete($entity, "goodToken");

        $this->assertTrue($result);
    }

    public function testDeleteFalse()
    {
        $token = new CsrfToken("delete_user", "goodToken");
        $entity = new User(new UserId(1), '', '');

        $this->csrfTokenManager->getToken('delete_user')->shouldBeCalled()->willReturn($token);

        $result = $this->crudManager->delete($entity, "anotherToken");

        $this->assertFalse($result);
    }
}
