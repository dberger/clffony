<?php


namespace App\Tests\App\Services\ResetPassword;

use App\Entity\User\User;
use App\Services\ResetPassword\ResetPasswordFormHandler;
use Doctrine\ORM\EntityManager;
use MsgPhp\User\Command\ChangeUserCredentialCommand;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\TraceableMessageBus;

class ResetPasswordFormHandlerTest extends TestCase
{
    /**
     * @var ResetPasswordFormHandler
     */
    private $resetPasswordFormHandler;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $request;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $bus;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $em;

    protected function setUp()
    {
        $this->request = $this->prophesize(RequestStack::class);
        $this->bus = $this->prophesize(TraceableMessageBus::class);
        $this->em = $this->prophesize(EntityManager::class);
        $this->resetPasswordFormHandler = new ResetPasswordFormHandler($this->request->reveal(), $this->bus->reveal(), $this->em->reveal());
    }

    public function testHandle()
    {
        $form = $this->prophesize(Form::class);
        $user = new User(new UserId(1), 'clffony@clffony.fr', 'test');
        $request = $this->prophesize(Request::class);
        $password = 'jeSuisUnMotDePasse';
        $data = ['password' => $password];

        $form->handleRequest($request)->shouldBeCalled();
        $form->getData()->shouldBeCalled()->willReturn($data);
        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);

        $msg = new ChangeUserCredentialCommand($user->getId(), ['password' => $password]);
        $this->bus->dispatch($msg)->shouldBeCalled()->willReturn(new Envelope($user));

        $result = $this->resetPasswordFormHandler->handle($form->reveal(), $request->reveal(), $user);

        $this->assertInstanceOf(User::class, $result);
    }

    public function testHandleValidNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $user = new User(new UserId(1), 'clffony@clffony.fr', 'test');
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->resetPasswordFormHandler->handle($form->reveal(), $request->reveal(), $user);

        $this->assertNull($result);
    }
}
