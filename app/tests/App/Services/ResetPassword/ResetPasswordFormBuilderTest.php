<?php


namespace App\Tests\App\Services\ResetPassword;

use App\Form\User\ResetPasswordType;
use App\Services\ResetPassword\ResetPasswordFormBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class ResetPasswordFormBuilderTest extends TestCase
{
    /**
     * @var ResetPasswordFormBuilder
     */
    private $resetPasswordFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->resetPasswordFormBuilder = new ResetPasswordFormBuilder($this->formFactory->reveal());
    }

    public function testGetForm()
    {
        $url = '#';
        $token = 'jeSuisUnToken';

        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->createNamed('', ResetPasswordType::class, null, ['action' => $url, ['token' => $token]])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->resetPasswordFormBuilder->getResetPasswordForm($url, $token);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
