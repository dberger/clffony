<?php

namespace App\Tests\App\Services\T2CLine;

use App\Entity\Lines\T2CLine;
use App\Services\T2CLine\T2CLinesCrudManager;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class T2CLinesCrudManagerTest extends TestCase
{
    /**
     * @var T2CLinesCrudManager
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $entityManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $eventDispatcher;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $csrfTokenManager;

    protected function setUp()
    {
        $this->entityManager = $this->prophesize(EntityManager::class);
        $this->eventDispatcher = $this->prophesize(EventDispatcher::class);
        $this->csrfTokenManager = $this->prophesize(CsrfTokenManager::class);
        $this->crudManager = new T2CLinesCrudManager($this->entityManager->reveal(), $this->eventDispatcher->reveal(), $this->csrfTokenManager->reveal());
    }

    public function testSave()
    {
        $entity = new T2CLine();
        $entity->setName('SuperStop');

        $this->entityManager->persist($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();

        $result = $this->crudManager->save($entity);

        $this->assertInstanceOf(T2CLine::class, $result);
    }

    public function testDeleteTrue()
    {
        $token = new CsrfToken("delete_t2cLine", "goodToken");
        $entity = new T2CLine();
        $entity->setName('SuperStop');

        $this->csrfTokenManager->getToken('delete_t2cLine')->shouldBeCalled()->willReturn($token);
        $this->entityManager->remove($entity)->shouldBeCalled();
        $this->entityManager->flush()->shouldBeCalled();
        $this->csrfTokenManager->removeToken('delete_t2cLine')->shouldBeCalled();

        $result = $this->crudManager->delete($entity, "goodToken");

        $this->assertTrue($result);
    }

    public function testDeleteFalse()
    {
        $token = new CsrfToken("delete_t2cLine", "goodToken");
        $entity = new T2CLine();
        $entity->setName('Super Stop');

        $this->csrfTokenManager->getToken('delete_t2cLine')->shouldBeCalled()->willReturn($token);

        $result = $this->crudManager->delete($entity, "anotherToken");

        $this->assertFalse($result);
    }
}
