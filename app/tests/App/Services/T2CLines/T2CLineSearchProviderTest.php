<?php
namespace App\Tests\App\Services\T2CLine;

use App\Entity\Lines\T2CLine;
use App\Repository\T2CLineRepository;
use App\Services\T2CLine\T2CLineSearchProvider;
use PHPUnit\Framework\TestCase;

class T2CLineSearchProviderTest extends TestCase
{
    /**
     * @var T2CLineSearchProvider
     */
    private $searchProvider;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $repository;

    protected function setUp()
    {
        $this->repository = $this->prophesize(T2CLineRepository::class);
        $this->searchProvider = new T2CLineSearchProvider($this->repository->reveal());
    }

    public function testFind()
    {
        $expected = new T2CLine();
        $id = 1;

        $this->repository->find($id)->shouldBeCalled()->willReturn($expected);

        $result = $this->searchProvider->find($id);

        $this->assertEquals($expected, $result);
    }

    public function testFindAll()
    {
        $arrayCollection = [];
        $this->repository->findAll()->shouldBeCalled()->willReturn($arrayCollection);
        $arrayCollection = $this->searchProvider->findAll();
        $this->assertInternalType('array', $arrayCollection);
    }
}
