<?php

namespace App\Tests\App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\User\User;
use App\Services\T2CFavorite\T2CFavoriteCrudManager;
use App\Services\T2CFavorite\T2CFavoriteFormHandler;
use MsgPhp\User\UserId;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class T2CFavoriteFormHandlerTest extends TestCase
{
    /**
     * @var T2CFavoriteFormHandler
     */
    private $formHandler;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $crudManager;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $requestStack;

    /**
     * @var User
     */
    private $user;


    protected function setUp()
    {
        $this->requestStack = $this->prophesize(RequestStack::class);
        $this->crudManager = $this->prophesize(T2CFavoriteCrudManager::class);
        $this->formHandler = new T2CFavoriteFormHandler($this->requestStack->reveal(), $this->crudManager->reveal());
        $this->user = new User(new UserId(1), 'test@clffony.fr', 'tests');
    }

    public function testHandleWithFormSubmittedAndValid()
    {
        $entity = new T2CFavorite();
        $entity->setName('Favori');

        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(true);
        $form->getData()->shouldBeCalled()->willReturn($entity);

        $this->crudManager->save($entity)->shouldBeCalled()->willReturn($entity);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal(), $this->user);

        $this->assertEquals($result, $result);
    }

    public function testHandleWithFormNotSubmitted()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal(), $this->user);

        $this->assertInternalType('array', $result);
    }

    public function testHandleWithFormSubmittedNotValid()
    {
        $form = $this->prophesize(Form::class);
        $request = $this->prophesize(Request::class);
        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->shouldBeCalled()->willReturn(true);
        $form->isValid()->shouldBeCalled()->willReturn(false);

        $result = $this->formHandler->handle($form->reveal(), $request->reveal(), $this->user);

        $this->assertNull($result);
    }
}
