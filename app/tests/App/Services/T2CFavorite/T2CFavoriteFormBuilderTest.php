<?php

namespace App\Tests\App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Form\T2CFavorite\T2CFavoriteModalType;
use App\Services\T2CFavorite\T2CFavoriteFormBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class T2CFavoriteFormBuilderTest extends TestCase
{
    /**
     * @var T2CFavoriteFormBuilder
     */
    private $t2cFavoriteFormBuilder;

    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->prophesize(FormFactory::class);
        $this->t2cFavoriteFormBuilder = new T2CFavoriteFormBuilder($this->formFactory->reveal());
    }

    public function testGetFormWithT2CFavorite()
    {
        $url = '#';

        $stopStart = (new T2CStop())->setName('StopDebut');
        $stopEnd = (new T2CStop())->setName('StopFin');
        $expectedForm = Argument::type(FormInterface::class);
        $this->formFactory->create(T2CFavoriteModalType::class, new T2CFavorite(), ['action' => $url, 'stopStart' => $stopStart, 'stopEnd' => $stopEnd])->shouldBeCalled()->willReturn($expectedForm);

        $result = $this->t2cFavoriteFormBuilder->getFavoriteForm($stopStart, $stopEnd, $url);
        $this->assertInstanceOf(get_class($expectedForm), $result);
    }
}
