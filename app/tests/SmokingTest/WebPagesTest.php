<?php

namespace App\Tests\App\SmokingTest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Client;

class WebPagesTest extends WebTestCase
{
    const GITLAB_TEST_URL = 'http://127.0.0.1:8000';
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@clffony.fr',
            'PHP_AUTH_PW'   => 'R^DcXznsXu73uFkZ',
        ]);
    }

    /**
     * @dataProvider urlProvider
     * @param $url
     */
    public function testPageIsSuccessful($url)
    {
        $this->client->request('GET', $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        yield [self::GITLAB_TEST_URL . '/'];
        yield [self::GITLAB_TEST_URL . '/admin/'];
        yield [self::GITLAB_TEST_URL . '/admin/stop'];
        yield [self::GITLAB_TEST_URL . '/admin/user'];
        yield [self::GITLAB_TEST_URL . '/admin/user/edit/1'];
        yield [self::GITLAB_TEST_URL . '/rest/stops'];
        yield [self::GITLAB_TEST_URL . '/rest/get-favorites'];
        yield [self::GITLAB_TEST_URL . '/rest/get-routes/1/2/5'];
    }
}
