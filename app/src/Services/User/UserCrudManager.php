<?php

namespace App\Services\User;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class UserCrudManager
 * @package App\Services\User
 */
class UserCrudManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * AccountStateCrudManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param CsrfTokenManagerInterface $csrfTokenManager
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    }

    /**
     * @param User $user
     * @param string $token
     * @return bool
     */
    public function delete(User $user, $token)
    {
        if ($this->csrfTokenManager->getToken('delete_user')->getValue() === $token) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
            $this->csrfTokenManager->removeToken('delete_user');
            return true;
        } else {
            return false;
        }
    }
}
