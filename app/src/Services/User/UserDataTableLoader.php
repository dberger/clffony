<?php

namespace App\Services\User;

use App\Entity\User\Role;
use App\Entity\User\User;
use Twig\Environment;

class UserDataTableLoader
{
    /**
     * @var UserSearchProvider
     */
    private $userSearchProvider;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * UserDataTableLoader constructor.
     * @param UserSearchProvider $userSearchProvider
     * @param Environment $twig
     */
    public function __construct(UserSearchProvider $userSearchProvider, Environment $twig)
    {
        $this->userSearchProvider = $userSearchProvider;
        $this->twig = $twig;
    }

    /**
     * @param User[] $data
     * @return array
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function formatUsersDataForDataTable($data)
    {
        $jsonData = [];

        foreach ($data as $row) {
            $params = [
                'entity' => $row,
                'route_name_edit' => 'admin_userpage_edit',
                'route_name_delete' => 'admin_userpage_delete',
                'label' => 'Utilisateur',
                'token' => 'delete_user',
                'css_selector' => 'user'
            ];

            $jsonData[] = [

                $row->getId(),
                $row->getEmail(),
                !$row->getRoles()->isEmpty() ? $row->getRoles()->first()->getRole()->getName() : Role::ROLE_USER,
                $this->twig->render(
                    'backOffice/buttons/user/buttons.html.twig',
                    $params
                )
            ];
        }
        return ['data' => $jsonData];
    }

    /**
     * @return array
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getAllUsers()
    {
        return $this->formatUsersDataForDataTable($this->userSearchProvider->findAll());
    }
}
