<?php

namespace App\Services\User;

use App\Entity\User\User;
use App\Model\User\UserSearch;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;

class UserSearchProvider
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserSearchProvider constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $id
     * @return null|User
     */
    public function find($id)
    {
        return $this->userRepository->find($id);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param UserSearch $search
     * @return ArrayCollection|null
     */
    public function search(UserSearch $search = null)
    {
        $search = !$search instanceof UserSearch ? new UserSearch() : $search;

        return $this->userRepository->getSearchQuery($search)->execute();
    }
}
