<?php

namespace App\Services\User;

use App\Entity\User\UserRole;
use App\Model\T2CRoute\T2CRouteSearch;
use MsgPhp\Domain\Message\MessageDispatchingTrait;
use MsgPhp\User\Command\ChangeUserCredentialCommand;
use MsgPhp\User\Command\CreateUserCommand;
use MsgPhp\User\Entity\User;
use MsgPhp\User\Infra\Doctrine\Repository\UserRepository;
use MsgPhp\User\Infra\Doctrine\Repository\UserRoleRepository;
use MsgPhp\User\Repository\UserRepositoryInterface;
use MsgPhp\User\Repository\UserRoleRepositoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * UserFormHandler Handler.
 */
class UserFormHandler
{
    use MessageDispatchingTrait {
        dispatch as protected;
    }

    /**
     * @var Request
     */
    private $request;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var UserCrudManager
     */
    private $userCrudManager;

    /**
     * @var UserRoleRepository
     */
    private $repository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserFormHandler constructor.
     * @param RequestStack $requestStack
     * @param MessageBusInterface $bus
     * @param UserCrudManager $userCrudManager
     * @param UserRoleRepositoryInterface $userRoleRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(RequestStack $requestStack, MessageBusInterface $bus, UserCrudManager $userCrudManager, UserRoleRepositoryInterface $userRoleRepository, UserRepositoryInterface $userRepository)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->bus = $bus;
        $this->userCrudManager = $userCrudManager;
        $this->repository = $userRoleRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return T2CRouteSearch|array
     */
    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->get('register')->getData();
                $this->bus->dispatch(new CreateUserCommand($data));
                $user = $this->userRepository->findByUsername($data['email']);
                // refacto crud
                $this->repository->save(new UserRole($user, $form->get('role')->getData()));
                return $form->getData();
            } else {
                return;
            }
        }


        return [];
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @param User $user
     * @return T2CRouteSearch|array
     */
    public function handleEdit(FormInterface $form, Request $request, User $user)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->bus->dispatch(new ChangeUserCredentialCommand($user->getId(), ['password' => $form->get('resetPassword')->getData()['password']]));
                return $form->getData();
            } else {
                return;
            }
        }


        return [];
    }
}
