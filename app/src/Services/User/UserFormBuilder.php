<?php

namespace App\Services\User;

use App\Entity\User\User;
use App\Form\User\UserEditType;
use App\Form\User\UserType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * UserFormBuilder Form Builder.
 */
class UserFormBuilder
{

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param String $url
     * @return FormInterface
     */
    public function getForm(String $url)
    {
        $form = $this->formFactory->create(UserType::class, null, [
            'action' => $url
        ]);

        return $form;
    }

    /**
     * @param String $url
     * @param User $user
     * @return FormInterface
     */
    public function getEditForm(String $url, User $user)
    {
        $form = $this->formFactory->create(UserEditType::class, null, [
            'action' => $url,
            'user' => $user
        ]);

        return $form;
    }
}
