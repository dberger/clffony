<?php

namespace App\Services\T2CRoute;

use App\Entity\User\User;
use App\Form\T2CRouteSearchType;
use App\Model\T2CRoute\T2CRouteSearch;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * T2CRouteFormBuilder Form Builder.
 */
class T2CRouteFormBuilder
{

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param String $url
     * @param User|null $user
     * @param T2CRouteSearch|null $t2CRouteSearch
     * @param bool $isFromRestRequest
     * @return FormInterface
     */
    public function getForm(String $url, User $user = null, T2CRouteSearch $t2CRouteSearch = null, $isFromRestRequest = false)
    {
        $form = $this->formFactory->create(T2CRouteSearchType::class, $t2CRouteSearch ? $t2CRouteSearch : new T2CRouteSearch(), [
            'action' => $url,
            'user' => $user,
            'csrf_protection' => !$isFromRestRequest
        ]);

        return $form;
    }
}
