<?php

namespace App\Services\T2CRoute;

use App\Model\T2CRoute\T2CRouteSearch;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * T2CRouteFormHandler Handler.
 */
class T2CRouteFormHandler
{

    /**
     * @var Request
     */
    private $request;

    /**
     * CommentFormHandler constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return T2CRouteSearch|array //TODO
     */
    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                return $form->getData();
            } else {
                return;
            }
        }


        return [];
    }
}
