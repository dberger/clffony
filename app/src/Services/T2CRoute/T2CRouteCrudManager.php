<?php

namespace App\Services\T2CRoute;

use App\Entity\Route\T2CRoute;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class T2CRouteCrudManager
 * @package App\Services\T2CRoutes
 */
class T2CRouteCrudManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * AccountStateCrudManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param CsrfTokenManagerInterface $csrfTokenManager
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param T2CRoute $t2cRoute
     * @return T2CRoute
     */
    public function save(T2CRoute $t2cRoute)
    {
        $this->entityManager->persist($t2cRoute);
        $this->entityManager->flush();
        return $t2cRoute;
    }

    /**
     * @param T2CRoute $t2cRoute
     * @param string $token
     * @return bool
     */
    public function delete(T2CRoute $t2cRoute, $token)
    {
        if ($this->csrfTokenManager->getToken('delete_t2cRoute')->getValue() === $token) {
            $this->entityManager->remove($t2cRoute);
            $this->entityManager->flush();
            $this->csrfTokenManager->removeToken('delete_t2cRoute');
            return true;
        } else {
            return false;
        }
    }
}
