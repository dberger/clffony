<?php

namespace App\Services\T2CStop;

use App\Entity\Stops\T2CStop;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class T2CStopCrudManager
 * @package App\Services\T2CStops
 */
class T2CStopCrudManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * AccountStateCrudManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param CsrfTokenManagerInterface $csrfTokenManager
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param T2CStop $t2cStop
     * @return T2CStop
     */
    public function save(T2CStop $t2cStop)
    {
        $this->entityManager->persist($t2cStop);
        $this->entityManager->flush();
        return $t2cStop;
    }

    /**
     * @param T2CStop $t2cStop
     * @param string $token
     * @return bool
     */
    public function delete(T2CStop $t2cStop, $token)
    {
        if ($this->csrfTokenManager->getToken('delete_t2cStop')->getValue() === $token) {
            $this->entityManager->remove($t2cStop);
            $this->entityManager->flush();
            $this->csrfTokenManager->removeToken('delete_t2cStop');
            return true;
        } else {
            return false;
        }
    }
}
