<?php

namespace App\Services\T2CStop;

use App\Entity\Stops\T2CStop;

class T2CStopDataTableLoader
{
    /**
     * @var T2CStopSearchProvider
     */
    private $t2cStopSearchProvider;

    /**
     * T2CStopDataTableLoader constructor.
     * @param T2CStopSearchProvider $t2cStopSearchProvider
     */
    public function __construct(T2CStopSearchProvider $t2cStopSearchProvider)
    {
        $this->t2cStopSearchProvider = $t2cStopSearchProvider;
    }

    /**
     * @param T2CStop[] $data
     * @return array
     */
    private function formatStopsDataForDataTable($data)
    {
        $jsonData = [];

        foreach ($data as $row) {
            $jsonData[] = [

                $row->getId(),
                $row->getName(),
                $row->getLine() ? $row->getLine()->getName(): '-',
            ];
        }
        return ['data' => $jsonData];
    }

    /**
     * @return array
     */
    public function getAllStops()
    {
        return $this->formatStopsDataForDataTable($this->t2cStopSearchProvider->findAll());
    }
}
