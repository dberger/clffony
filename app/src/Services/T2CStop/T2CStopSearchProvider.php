<?php

namespace App\Services\T2CStop;

use App\Repository\T2CStopRepository;

class T2CStopSearchProvider
{
    /**
     * @var T2CStopRepository
     */
    protected $t2CStopRepository;

    /**
     * T2CStopSearchProvider constructor.
     * @param T2CStopRepository $t2CStopRepository
     */
    public function __construct(T2CStopRepository $t2CStopRepository)
    {
        $this->t2CStopRepository = $t2CStopRepository;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->t2CStopRepository->find($id);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->t2CStopRepository->findAll();
    }
}
