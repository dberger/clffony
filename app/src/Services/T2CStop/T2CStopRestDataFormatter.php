<?php

namespace App\Services\T2CStop;

use App\Entity\Stops\T2CStop;
use App\Services\Base\BaseRestDataFormatter;

/**
 * Class T2CStopRestDataFormatter
 * @package App\Services\T2CStop
 */
class T2CStopRestDataFormatter implements BaseRestDataFormatter
{
    /**
     * @param T2CStop[] $objects
     * @return array
     */
    public function formatData(array $objects)
    {
        $result = [];
        if (is_array($objects)) {
            foreach ($objects as $object) {
                $result[] = [
                    'idStop' => $object->getId(),
                    'stopName' => $object->getName()
                ];
            }
        }
        return $result;
    }
}