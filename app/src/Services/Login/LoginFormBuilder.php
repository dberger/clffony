<?php

namespace App\Services\Login;

use App\Form\User\LoginType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginFormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     * @param AuthenticationUtils $authenticationUtils
     */
    public function __construct(FormFactoryInterface $formFactory, AuthenticationUtils $authenticationUtils)
    {
        $this->formFactory = $formFactory;
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @param $url
     * @return FormInterface
     */
    public function getLoginForm(String $url)
    {
        $form = $this->formFactory->createNamed('', LoginType::class, ['email' => $this->authenticationUtils->getLastUsername()], [
            'action' => $url
        ]);

        return $form;
    }
}
