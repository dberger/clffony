<?php

namespace App\Services\T2CLine;

use App\Entity\Lines\T2CLine;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class T2CLinesCrudManager
 * @package App\Services\T2CLines
 */
class T2CLinesCrudManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * AccountStateCrudManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param CsrfTokenManagerInterface $csrfTokenManager
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param T2CLine $t2cLine
     * @return T2CLine
     */
    public function save(T2CLine $t2cLine)
    {
        $this->entityManager->persist($t2cLine);
        $this->entityManager->flush();
        return $t2cLine;
    }

    /**
     * @param T2CLine $t2cLine
     * @param string $token
     * @return bool
     */
    public function delete(T2CLine $t2cLine, $token)
    {
        if ($this->csrfTokenManager->getToken('delete_t2cLine')->getValue() === $token) {
            $this->entityManager->remove($t2cLine);
            $this->entityManager->flush();
            $this->csrfTokenManager->removeToken('delete_t2cLine');
            return true;
        } else {
            return false;
        }
    }
}
