<?php

namespace App\Services\T2CLine;

use App\Entity\Lines\T2CLine;
use App\Repository\T2CLineRepository;

class T2CLineSearchProvider
{
    /**
     * @var T2CLineRepository
     */
    protected $t2CLineRepository;

    /**
     * T2CStopSearchProvider constructor.
     * @param T2CLineRepository $t2CLineRepository
     */
    public function __construct(T2CLineRepository $t2CLineRepository)
    {
        $this->t2CLineRepository = $t2CLineRepository;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->t2CLineRepository->find($id);
    }

    /**
     * @return T2CLine[]
     */
    public function findAll()
    {
        return $this->t2CLineRepository->findAll();
    }
}
