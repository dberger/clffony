<?php

namespace App\Services\Rest;

use App\Entity\Stops\BaseStop;
use App\Model\T2CRoute\T2CRouteSearch;
use App\Services\T2CRoute\T2CRouteFormBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RestFormValidator
 * @package App\Services\Rest
 */
class RestFormValidator
{
    /**
     * @var T2CRouteFormBuilder
     */
    private $t2CRouteFormBuilder;

    public function __construct(T2CRouteFormBuilder $t2CRouteFormBuilder)
    {
        $this->t2CRouteFormBuilder = $t2CRouteFormBuilder;
    }

    /**
     * @param Request $request
     * @param BaseStop $stopStart
     * @param BaseStop $stopEnd
     * @param int $nbResults
     * @return T2CRouteSearch | null
     */
    public function validateRoutesSearch(Request $request, BaseStop $stopStart, BaseStop $stopEnd, int $nbResults)
    {
        $search = (new T2CRouteSearch())
            ->setStopStart($stopStart)
            ->setStopEnd($stopEnd)
            ->setMaxResults($nbResults);

        $form = $this->t2CRouteFormBuilder->getForm('#', null, $search, true);
        $form->handleRequest($request);
        $form->get('stopStart')->submit($stopStart->getId());
        $form->get('stopEnd')->submit($stopEnd->getId());
        $form->get('maxResults')->submit($nbResults);
        $form->submit([], false);

        if ($form->isValid()) {
            return $form->getData();
        };

        return null;
    }
}