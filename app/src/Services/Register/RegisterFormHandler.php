<?php


namespace App\Services\Register;

use Doctrine\ORM\EntityManagerInterface;
use MsgPhp\Domain\Message\MessageDispatchingTrait;
use MsgPhp\User\Command\CreateUserCommand;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;

class RegisterFormHandler
{
    use MessageDispatchingTrait {
        dispatch as protected;
    }

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CommentFormHandler constructor.
     * @param RequestStack $requestStack
     * @param MessageBusInterface $bus
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $requestStack, MessageBusInterface $bus, EntityManagerInterface $entityManager)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->bus = $bus;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return bool|null
     */
    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->bus->dispatch(new CreateUserCommand($form->getData()));

            return true;
        }

        return null;
    }
}
