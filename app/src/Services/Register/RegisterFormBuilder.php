<?php


namespace App\Services\Register;

use App\Form\User\RegisterType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class RegisterFormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $url
     * @return FormInterface
     */
    public function getRegisterForm(String $url)
    {
        $form = $this->formFactory->createNamed('', RegisterType::class, null, [
            'action' => $url
        ]);

        return $form;
    }
}
