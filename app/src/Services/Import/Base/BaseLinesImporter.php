<?php

namespace App\Services\Import\Base;

use App\Entity\Lines\BaseLine;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface BaseLinesImporter
 * @package App\Services\Import\Base
 */
interface BaseLinesImporter
{
    /**
     * @return BaseLine[]
     */
    public function importLines() : ArrayCollection;
}
