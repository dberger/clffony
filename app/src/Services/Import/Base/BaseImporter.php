<?php

namespace App\Services\Import\Base;

use App\Entity\Lines\BaseLine;

/**
 * Class BaseImporter
 * @package App\Services\Import\Base
 */
abstract class BaseImporter
{
    /**
     * @var BaseLinesImporter
     */
    protected $baseLinesImporter;

    /**
     * @var BaseStopsImporter
     */
    protected $baseStopsImporter;

    /**
     * BaseImporter constructor.
     * @param BaseLinesImporter $baseLinesImporter
     * @param BaseStopsImporter $baseStopsImporter
     */
    public function __construct(BaseLinesImporter $baseLinesImporter, BaseStopsImporter $baseStopsImporter)
    {
        $this->baseLinesImporter = $baseLinesImporter;
        $this->baseStopsImporter = $baseStopsImporter;
    }

    /**
     * Import data
     * @return BaseLine[]
     */
    abstract public function import();
}
