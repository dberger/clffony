<?php

namespace App\Services\Import\Base;

use App\Entity\Lines\BaseLine;
use App\Entity\Stops\BaseStop;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface BaseStopsImporter
 * @package App\Services\Import\Base
 */
interface BaseStopsImporter
{
    /**
     * @param BaseLine $baseLine
     * @return BaseStop[]
     */
    public function importStops(BaseLine $baseLine) : ArrayCollection;
}
