<?php

namespace App\Services\Import\T2C;

use App\Entity\Lines\BaseLine;
use App\Entity\Stops\T2CStop;
use App\Services\Import\Base\BaseStopsImporter;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class T2CStopsImporter
 * @package App\Services\Import\T2C
 */
class T2CStopsImporter implements BaseStopsImporter
{

    /**
     * Import T2C stops from a specific line
     * @param BaseLine $baseLine
     * @return ArrayCollection
     */
    public function importStops(BaseLine $baseLine): ArrayCollection
    {
        $data = json_decode(exec('curl -s \'https://www.t2c.fr/horaires-aux-arrets?ajax_form=1&_wrapper_format=drupal_ajax&_wrapper_format=drupal_ajax\' -H \'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0\' -H \'Accept: application/json, text/javascript, */*; q=0.01\' -H \'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\' --compressed -H \'Referer: https://www.t2c.fr/horaires-aux-arrets\' -H \'Content-Type: application/x-www-form-urlencoded; charset=UTF-8\' -H \'X-Requested-With: XMLHttpRequest\' -H \'Connection: keep-alive\' -H \'Cookie: bandeau_alerte=1; _ga=GA1.2.1971586758.1547209329; _gid=GA1.2.764385863.1547209329; tarteaucitron=!analytics=wait!addthis=wait; SSESScd238e93e2dae06f38dc0ad3ae037d61=nARxg68ExwQqHFT4Rx6pp5dA1Tb-rQJtuvk7udatgzQ\' --data \'periodes=Horaires+hiver+du+27+ao%C3%BBt+2018+au+30+juin+2019+&lignes=1&directions=La+Pardieu+Gare&arrets=0&form_build_id=form-XTntiVdXLyaVR5GWnYvzQWTHaAAeU6WJe2t2W7WBs5s&form_id=ajax_horairesauxarrets_form&_triggering_element_name=directions&_drupal_ajax=1&ajax_page_state%5Btheme%5D=t2c&ajax_page_state%5Btheme_token%5D=&ajax_page_state%5Blibraries%5D=%2Fsites%2Fall%2Fthemes%2Ft2c%2Fcss%2Ftarteaucitron%2Ccore%2Fhtml5shiv%2Cgoogle_analytics%2Fgoogle_analytics%2Csystem%2Fbase%2Ct2c%2Ft2c-css%2Ct2c%2Ft2c-js%2Ctarteaucitron%2Ftarteaucitron.code\''))[2]->data;
        $preFormattedData = explode('option', $data);
        array_shift($preFormattedData);
        array_shift($preFormattedData);
        array_pop($preFormattedData);
        $stops = new ArrayCollection();
        foreach ($preFormattedData as $row) {
            $row = explode('"', $row);
            if (isset($row[1])) {
                $stops->add((new T2CStop())->setName($row[1])->setLine($baseLine));
            }
        }
        return $stops;
    }
}
