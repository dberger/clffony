<?php

namespace App\Services\Import\T2C;

use App\Entity\Lines\T2CLine;
use App\Entity\Stops\T2CStop;
use App\Services\T2CLine\T2CLineSearchProvider;
use App\Services\T2CStop\T2CStopCrudManager;

class T2CUpdater
{
    /**
     * @var T2CImporter
     */
    private $importer;

    /**
     * @var T2CLineSearchProvider
     */
    private $t2CLineSearchProvider;

    /**
     * @var T2CStopCrudManager
     */
    private $t2CStopCrudManager;

    /**
     * T2CUpdater constructor.
     * @param T2CImporter $importer
     * @param T2CLineSearchProvider $t2CLineSearchProvider
     * @param T2CStopCrudManager $t2CStopCrudManager
     */
    public function __construct(T2CImporter $importer, T2CLineSearchProvider $t2CLineSearchProvider, T2CStopCrudManager $t2CStopCrudManager)
    {
        $this->importer = $importer;
        $this->t2CLineSearchProvider = $t2CLineSearchProvider;
        $this->t2CStopCrudManager = $t2CStopCrudManager;
    }

    /**
     * Gets the T2C data
     * @return T2CLine[]
     */
    public function updateDatabase()
    {
        $sortedLocalLines = [];
        $sortedLocalStops = [];
        $remoteLines = $this->importer->import();
        $localLines = $this->t2CLineSearchProvider->findAll();

        $stopsToAdd = [];

        foreach ($localLines as $line) {
            $sortedLocalLines[$line->getName()] = $line;
            foreach ($line->getStops() as $stop) {
                $sortedLocalStops[$line->getName()][$stop->getName()] = $stop;
            }
        }

        foreach ($remoteLines as $line) {
            if (isset($sortedLocalLines[$line->getName()])) {
                foreach ($line->getStops() as $stop) {
                    if (!isset($sortedLocalStops[$line->getName()][$stop->getName()])) {
                        $stopsToAdd[] = (new T2CStop())->setName($stop->getName())->setLine($sortedLocalLines[$line->getName()]);
                    }
                }
            }
        }

        foreach ($stopsToAdd as $stop) {
            $this->t2CStopCrudManager->save($stop);
        }


        return $stopsToAdd;
    }
}
