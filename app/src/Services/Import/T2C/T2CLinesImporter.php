<?php

namespace App\Services\Import\T2C;

use App\Entity\Lines\T2CLine;
use App\Services\Import\Base\BaseLinesImporter;
use Doctrine\Common\Collections\ArrayCollection;

class T2CLinesImporter implements BaseLinesImporter
{

    /**
     * Import T2C lines
     * @return ArrayCollection
     */
    public function importLines(): ArrayCollection
    {
        $collections = new ArrayCollection();
        $collections->add((new T2CLine())->setName('Ligne A')->setType(T2CLine::T2C_TYPE_TRAM));
        return $collections;
    }
}
