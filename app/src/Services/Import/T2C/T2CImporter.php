<?php

namespace App\Services\Import\T2C;

use App\Entity\Lines\T2CLine;
use App\Services\Import\Base\BaseImporter;
use App\Services\Import\Base\BaseLinesImporter;
use App\Services\Import\Base\BaseStopsImporter;

class T2CImporter extends BaseImporter
{


    /**
     * T2CImporter constructor.
     * @param BaseLinesImporter $baseLinesImporter
     * @param BaseStopsImporter $baseStopsImporter
     */
    public function __construct(BaseLinesImporter $baseLinesImporter, BaseStopsImporter $baseStopsImporter)
    {
        parent::__construct($baseLinesImporter, $baseStopsImporter);
    }

    /**
     * Gets the T2C data
     * @return T2CLine[]
     */
    public function import()
    {
        /** @var T2CLine[] $lines */
        $lines = $this->baseLinesImporter->importLines();

        foreach ($lines as $line) {
            foreach ($this->baseStopsImporter->importStops($line) as $stop) {
                $line->addStop($stop);
            };
        }

        return $lines;
    }
}
