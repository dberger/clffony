<?php


namespace App\Services\ResetPassword;

use App\Form\User\ResetPasswordType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class ResetPasswordFormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param String $url
     * @param String $token
     * @return FormInterface
     */
    public function getResetPasswordForm(String $url, String $token)
    {
        $form = $this->formFactory->createNamed('', ResetPasswordType::class, null, [
            'action' => $url,
            ['token' => $token]
        ]);

        return $form;
    }
}
