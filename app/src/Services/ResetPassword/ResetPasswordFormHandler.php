<?php


namespace App\Services\ResetPassword;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use MsgPhp\Domain\Message\MessageDispatchingTrait;
use MsgPhp\User\Command\ChangeUserCredentialCommand;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;

class ResetPasswordFormHandler
{
    use MessageDispatchingTrait {
        dispatch as protected;
    }

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CommentFormHandler constructor.
     * @param RequestStack $requestStack
     * @param MessageBusInterface $bus
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $requestStack, MessageBusInterface $bus, EntityManagerInterface $entityManager)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->bus = $bus;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @param User $user
     * @return User
     */
    public function handle(FormInterface $form, Request $request, User $user)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->bus->dispatch(new ChangeUserCredentialCommand($user->getId(), ['password' => $form->getData()['password']]));

            return $user;
        }

        return null;
    }
}
