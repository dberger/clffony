<?php

namespace App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\User\User;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * T2CFavoriteFormHandler Handler.
 */
class T2CFavoriteFormHandler
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var T2CFavoriteCrudManager
     */
    private $t2CFavoriteCrudManager;

    /**
     * CommentFormHandler constructor.
     * @param RequestStack $requestStack
     * @param T2CFavoriteCrudManager $t2CFavoriteCrudManager
     */
    public function __construct(RequestStack $requestStack, T2CFavoriteCrudManager $t2CFavoriteCrudManager)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->t2CFavoriteCrudManager = $t2CFavoriteCrudManager;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @param User $user
     * @return T2CFavorite|array
     */
    public function handle(FormInterface $form, Request $request, User $user)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var T2CFavorite $favorite */
                $favorite = $form->getData();
                $favorite->setUser($user);
                return $this->t2CFavoriteCrudManager->save($favorite);
            } else {
                return;
            }
        }


        return [];
    }
}
