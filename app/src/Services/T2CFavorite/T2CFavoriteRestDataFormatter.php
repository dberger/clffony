<?php


namespace App\Services\T2CFavorite;


use App\Entity\Favorite\T2CFavorite;
use App\Services\Base\BaseRestDataFormatter;

class T2CFavoriteRestDataFormatter implements BaseRestDataFormatter
{
    const KEY_ID_FAVORITE = 'idFavorite';
    const KEY_FAVORITE_NAME = 'favoriteName';
    const KEY_FAVORITE_CONTENT = 'favoriteContent';

    /**
     * Format the objects as Json data
     * @param T2CFavorite[] $objects
     * @return array
     */
    public function formatData(?array $objects)
    {
        $result = [];
        if (is_array($objects)) {
            foreach ($objects as $object) {
                $result[] = [
                    self::KEY_ID_FAVORITE => $object->getId(),
                    self::KEY_FAVORITE_NAME => $object->getName(),
                    self::KEY_FAVORITE_CONTENT => $object->getStopInfos()
                ];
            }
        }
        return $result;
    }
}