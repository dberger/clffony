<?php

namespace App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Form\T2CFavorite\T2CFavoriteModalType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class T2CFavoriteFormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param T2CStop $start
     * @param T2CStop $end
     * @param $url
     * @return FormInterface
     */
    public function getFavoriteForm(?T2CStop $start, ?T2CStop $end, $url)
    {
        $form = $this->formFactory->create(T2CFavoriteModalType::class, new T2CFavorite(), [
            'action' => $url,
            'stopStart' => $start,
            'stopEnd' => $end
        ]);

        return $form;
    }
}
