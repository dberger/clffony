<?php

namespace App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Lines\T2CLine;
use App\Entity\User\User;
use App\Model\T2CFavorite\T2CFavoriteSearch;
use App\Repository\T2CFavoriteRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class T2CFavoriteSearchProvider
{
    /**
     * @var T2CFavoriteRepository
     */
    protected $t2cFavoriteRepository;

    /**
     * T2CStopSearchProvider constructor.
     * @param T2CFavoriteRepository $t2cFavoriteRepository
     */
    public function __construct(T2CFavoriteRepository $t2cFavoriteRepository)
    {
        $this->t2cFavoriteRepository = $t2cFavoriteRepository;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function find($id): ?T2CFavorite
    {
        return $this->t2cFavoriteRepository->find($id);
    }

    /**
     * @return T2CLine[]
     */
    public function findAll(): array
    {
        return $this->t2cFavoriteRepository->findAll();
    }

    /**
     * @param T2CFavoriteSearch|null $t2CFavoriteSearch
     * @param User|null $user
     * @return T2CFavorite|null|array
     */
    public function search(?T2CFavoriteSearch $t2CFavoriteSearch, ?User $user): ?array
    {
        $search = $t2CFavoriteSearch ? $t2CFavoriteSearch : new T2CFavoriteSearch();
        $search->setUser($user);
        try {
            return $this->t2cFavoriteRepository->getSearchQuery($search)->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return $this->t2cFavoriteRepository->getSearchQuery($search)->execute();
        }
    }
}
