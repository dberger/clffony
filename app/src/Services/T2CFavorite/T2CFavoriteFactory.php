<?php

namespace App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Entity\User\User;

/**
 * Class T2CFavoriteCrudManager
 * @package App\Services\T2CRoutes
 */
class T2CFavoriteFactory
{
    /**
     * @param User $user
     * @param T2CStop $start
     * @param T2CStop $end
     * @param string $favoriteName
     * @return T2CFavorite
     */
    public function buildNewUserFavorite(User $user, T2CStop $start, T2CStop $end, string $favoriteName)
    {
        return (new T2CFavorite())->setStopStart($start)->setStopEnd($end)->setUser($user)->setName($favoriteName);
    }
}
