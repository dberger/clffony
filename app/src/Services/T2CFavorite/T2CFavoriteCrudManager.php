<?php

namespace App\Services\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class T2CFavoriteCrudManager
 * @package App\Services\T2CRoutes
 */
class T2CFavoriteCrudManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * AccountStateCrudManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param CsrfTokenManagerInterface $csrfTokenManager
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param T2CFavorite $t2CFavorite
     * @return T2CFavorite
     */
    public function save(T2CFavorite $t2CFavorite)
    {
        $this->entityManager->persist($t2CFavorite);
        $this->entityManager->flush();
        return $t2CFavorite;
    }

    /**
     * @param T2CFavorite $t2CFavorite
     * @param string $token
     * @return bool
     */
    public function delete(T2CFavorite $t2CFavorite, $token)
    {
        if ($this->csrfTokenManager->getToken('delete_t2cFavorite')->getValue() === $token) {
            $this->entityManager->remove($t2CFavorite);
            $this->entityManager->flush();
            $this->csrfTokenManager->removeToken('delete_t2cFavorite');
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param T2CFavorite $t2CFavorite
     * @return bool
     */
    public function deleteFromRest(T2CFavorite $t2CFavorite)
    {
        $this->entityManager->remove($t2CFavorite);
        $this->entityManager->flush();
        return true;
    }
}
