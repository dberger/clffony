<?php

namespace App\Services\RemoteIO;

use App\Entity\Stops\BaseStop;
use App\Model\T2CRoute\T2CRouteSearch;

/**
 * Class CurlRequestBuilder
 * @package App\Services\RemoteIO
 */
class CurlRequestBuilder
{

    /**
     * @param T2CRouteSearch $search
     * @return array
     * @throws \Exception
     */
    public function findRoutesFromNow(T2CRouteSearch $search)
    {
        return [
            'method' => CurlRequestQuery::METHOD_GET,
            'data' => [
                'dpt' => $search->getStopStart()->getName(),
                'apt' => $search->getStopEnd()->getName(),
                'dy' => (new \DateTime())->format('Y-m-d')
            ]
        ];
    }
}
