<?php

namespace App\Services\RemoteIO;

use App\Entity\Route\T2CRoute;
use App\Model\T2CRoute\T2CRouteSearch;

class CurlRequestFormatter
{
    /**
     * @param array $results
     * @param T2CRouteSearch $search
     * @return array
     */
    public function convertResultsToRouteObject(?array $results, T2CRouteSearch $search) : ?array
    {
        $nbFormattedRows = 0;
        if (isset($results['board'])) {
            $routes = [];
            foreach ($results['board'] as $row) {
                if (isset($row['@attributes'])) {
                    $routes[] = (new T2CRoute())
                        ->setStopStart($search->getStopStart())
                        ->setStopEnd($search->getStopEnd())
                        ->setHourStart(\DateTime::createFromFormat('H:i', $row['@attributes']['dt']))
                        ->setHourEnd(\DateTime::createFromFormat('H:i', $row['@attributes']['at']));
                }
                if ($search->getMaxResults()) {
                    $nbFormattedRows++;
                    if ($nbFormattedRows == $search->getMaxResults()) {
                        return $routes;
                    }
                }
            }
            return $routes;
        }
        return null;
    }

    /**
     * @param array $results
     * @param T2CRouteSearch $search
     * @return array
     * @throws \Exception
     */
    public function convertResultsToArray(?array $results, T2CRouteSearch $search) : ?array
    {
        $nbFormattedRows = 0;
        if (isset($results['board'])) {
            $routes = [];
            foreach ($results['board'] as $row) {
                if (isset($row['@attributes'])) {
                    $hourStart = \DateTime::createFromFormat('H:i', $row['@attributes']['dt']);
                    $hourEnd = \DateTime::createFromFormat('H:i', $row['@attributes']['at']);
                    $routes[] = [
                        'stopStart' => $search->getStopStart()->getName(),
                        'stopEnd' => $search->getStopEnd()->getName(),
                        'hourStart' => $hourStart->format('H:i'),
                        'hourEnd' => $hourEnd->format('H:i'),
                        'countdown' => ($hourStart->getTimestamp() - time()) * 1000
                    ];
                }
                if ($search->getMaxResults()) {
                    $nbFormattedRows++;
                    if ($nbFormattedRows == $search->getMaxResults()) {
                        return $routes;
                    }
                }
            }
            return $routes;
        }
        return null;
    }
}
