<?php

namespace App\Services\RemoteIO;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CurlRequestQuery
 * @package App\Services\RemoteIO
 */
class CurlRequestQuery
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var string
     */
    private $t2cUrl;

    /**
     * CurlRequestBuilder constructor.
     * @param RequestStack $request
     * @param string $t2cUrl
     */
    public function __construct(RequestStack $request, string $t2cUrl)
    {
        $this->request = $request;
        $this->t2cUrl = $t2cUrl;
    }

    /**
     * Curl request builder
     * @param array $params
     * @return null
     */
    public function query($params = [])
    {
        if (!empty($params)) {
            $userAgent = $this->request->getCurrentRequest()->headers->get('User-Agent');

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);

            if (isset($params['method'])) {
                switch ($params['method']) {
                    case self::METHOD_POST:
                        curl_setopt($curl, CURLOPT_POST, 1);
                        if (isset($params['data']) && $params['data']) {
                            curl_setopt($curl, CURLOPT_POSTFIELDS, $params['data']);
                        }
                        break;
                    default:
                        if (isset($params['data']) && $params['data']) {
                            $this->t2cUrl = sprintf("%s&%s", $this->t2cUrl, http_build_query($params['data'], null, '&', PHP_QUERY_RFC3986));
                        }
                }
            }

            // OPTIONS:
            curl_setopt($curl, CURLOPT_URL, $this->t2cUrl);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/xml',
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            // EXECUTE:
            $result = curl_exec($curl);

            if (!$result) {
                return null;
            }

            curl_close($curl);

            try {
                $xml = simplexml_load_string($result);
                $json = json_encode($xml);
                $array = json_decode($json, true);
            } catch (\Exception $e) {
                return null;
            }

            return $array;
        } else {
            return null;
        }
    }
}
