<?php

namespace App\Services\ForgotPassword;

use App\Form\User\ForgotPasswordType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class ForgotPasswordFormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $url
     * @return FormInterface
     */
    public function getForgotPasswordForm(String $url)
    {
        $form = $this->formFactory->createNamed('', ForgotPasswordType::class, null, [
            'action' => $url
        ]);

        return $form;
    }
}
