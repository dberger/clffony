<?php


namespace App\Services\ForgotPassword;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use MsgPhp\Domain\Message\MessageDispatchingTrait;
use MsgPhp\User\Command\RequestUserPasswordCommand;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;

class ForgotPasswordFormHandler
{
    use MessageDispatchingTrait {
        dispatch as protected;
    }

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CommentFormHandler constructor.
     * @param RequestStack $requestStack
     * @param MessageBusInterface $bus
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $requestStack, MessageBusInterface $bus, EntityManagerInterface $entityManager)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->bus = $bus;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return User
     */
    public function handle(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['credential.email' => $form->getData()['email']]);
            $this->bus->dispatch(new RequestUserPasswordCommand($user->getId()));

            return $user;
        }

        return null;
    }
}
