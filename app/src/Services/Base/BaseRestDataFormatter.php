<?php

namespace App\Services\Base;

/**
 * Interface BaseRestDataFormatter
 * @package App\Services\Base
 */
interface BaseRestDataFormatter
{
    /**
     * Format the objects as Json data
     * @param array $objects
     * @return array
     */
    public function formatData(array $objects);
}