<?php

namespace App\Repository;

use App\Model\T2CFavorite\T2CFavoriteSearch;
use Doctrine\ORM\EntityRepository;

/**
 * Class T2CFavoriteRepository
 * @package App\Repository
 */
class T2CFavoriteRepository extends EntityRepository
{
    /**
     * @param T2CFavoriteSearch $search
     * @param bool $isQueryBuilderNeeded
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    public function getSearchQuery(T2CFavoriteSearch $search, $isQueryBuilderNeeded = false)
    {
        $query = $this->createQueryBuilder('t');

        if ($search->getId()) {
            $query->andWhere('t.id = :id')
                ->setParameter('id', $search->getId());
        }

        if ($search->getUser()) {
            $query->andWhere('t.user = :user')
                ->setParameter('user', $search->getUser());
        }

        return $isQueryBuilderNeeded ? $query : $query->getQuery();
    }
}
