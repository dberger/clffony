<?php


namespace App\Controller\FrontOffice;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Model\Response\AjaxResponse;
use App\Services\RemoteIO\CurlRequestBuilder;
use App\Services\RemoteIO\CurlRequestFormatter;
use App\Services\RemoteIO\CurlRequestQuery;
use App\Services\Rest\RestFormValidator;
use App\Services\T2CFavorite\T2CFavoriteCrudManager;
use App\Services\T2CFavorite\T2CFavoriteFactory;
use App\Services\T2CFavorite\T2CFavoriteRestDataFormatter;
use App\Services\T2CFavorite\T2CFavoriteSearchProvider;
use App\Services\T2CStop\T2CStopRestDataFormatter;
use App\Services\T2CStop\T2CStopSearchProvider;
use App\Services\User\UserSearchProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RestController
 * @package App\Controller\FrontOffice
 * @Route("rest")
 */
class RestController extends AbstractController
{
    /**
     * Return all the stops from the database as json
     *
     * @Route("/stops", name="rest_get_stops", methods={"GET"})
     * @param T2CStopSearchProvider $t2CStopSearchProvider
     * @param T2CStopRestDataFormatter $t2CStopRestDataFormatter
     * @return JsonResponse
     */
    public function listStops(T2CStopSearchProvider $t2CStopSearchProvider, T2CStopRestDataFormatter $t2CStopRestDataFormatter)
    {
        return new JsonResponse($t2CStopRestDataFormatter->formatData($t2CStopSearchProvider->findAll()));
    }

    /**
     * HomePage Ajax Block
     *
     * @Route("/get-routes/{stopStart}/{stopEnd}/{nbResult}", name="rest_get_routes", methods={"GET"})
     * @param Request $request
     * @param CurlRequestQuery $curlRequestQuery
     * @param CurlRequestBuilder $curlRequestBuilder
     * @param CurlRequestFormatter $curlRequestFormatter
     * @param T2CStop $stopStart
     * @param T2CStop $stopEnd
     * @param int $nbResult
     * @param RestFormValidator $restFormValidator
     * @return Response
     * @throws \Exception
     */
    public function getRoutesAction(
        Request $request,
        CurlRequestQuery $curlRequestQuery,
        CurlRequestBuilder $curlRequestBuilder,
        CurlRequestFormatter $curlRequestFormatter,
        T2CStop $stopStart,
        T2CStop $stopEnd,
        RestFormValidator $restFormValidator,
        int $nbResult = 5
    )
    {
        if ($search = $restFormValidator->validateRoutesSearch($request, $stopStart, $stopEnd, $nbResult)) {
            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK);
            $data = $curlRequestQuery->query($curlRequestBuilder->findRoutesFromNow($search));
            $routes = $curlRequestFormatter->convertResultsToArray($data, $search);
            $ajaxResponse->setData($routes);
            return $ajaxResponse->formatResponseContent();
        }

        return (new AjaxResponse(null, Response::HTTP_NOT_FOUND))->formatResponseContent();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/get-favorites", name="rest_get_favorite", methods={"GET"})
     * Return all the users favorites as json
     * @param T2CFavoriteSearchProvider $t2CFavoriteSearchProvider
     * @param T2CFavoriteRestDataFormatter $t2CFavoriteRestDataFormatter
     * @param UserSearchProvider $userSearchProvider
     * @return AjaxResponse
     */
    public function getUserFavorites(T2CFavoriteSearchProvider $t2CFavoriteSearchProvider, T2CFavoriteRestDataFormatter $t2CFavoriteRestDataFormatter, UserSearchProvider $userSearchProvider)
    {
        if ($msgPhpUser = $this->getUser()) {
            return (new AjaxResponse(
                $t2CFavoriteRestDataFormatter->formatData(
                    $t2CFavoriteSearchProvider->search(null, $userSearchProvider->find($msgPhpUser->getUserId()))
                ), Response::HTTP_OK
            ))->formatResponseContent();
        }
        return (new AjaxResponse(null, Response::HTTP_FORBIDDEN))->formatResponseContent();

    }

    /**
     * Add a favorite to the user
     * @IsGranted("ROLE_USER")
     * @Route("/add-favorite/{stopStart}/{stopEnd}/{favoriteName}", name="rest_add_favorite", methods={"POST"})
     * @param T2CStop $stopStart
     * @param T2CStop $stopEnd
     * @param string $favoriteName
     * @param UserSearchProvider $userSearchProvider
     * @param T2CFavoriteCrudManager $t2CFavoriteCrudManager
     * @param T2CFavoriteFactory $t2CFavoriteFactory
     * @return AjaxResponse
     */
    public function addUserFavorite(
        T2CStop $stopStart,
        T2CStop $stopEnd,
        string $favoriteName,
        UserSearchProvider $userSearchProvider,
        T2CFavoriteCrudManager $t2CFavoriteCrudManager,
        T2CFavoriteFactory $t2CFavoriteFactory
    )
    {
        if ($msgPhpUser = $this->getUser()) {
            if ($t2CFavoriteCrudManager->save($t2CFavoriteFactory->buildNewUserFavorite($userSearchProvider->find($msgPhpUser->getUserId()), $stopStart, $stopEnd, $favoriteName))) {
                return (new AjaxResponse(null, Response::HTTP_OK, null, null, 'success', 'Le favori a bien été ajouté'))->formatResponseContent();
            }
        }
        return (new AjaxResponse(null, Response::HTTP_FORBIDDEN, null, null, 'success', 'Le favori n\'a pas été ajouté'))->formatResponseContent();
    }

    /**
     * Add a favorite to the user
     * @IsGranted("ROLE_USER")
     * @Route("/delete-favorite/{favorite}", name="rest_delete_favorite", methods={"DELETE"})
     * @param T2CFavorite $favorite
     * @param T2CFavoriteCrudManager $t2CFavoriteCrudManager
     * @param UserSearchProvider $userSearchProvider
     * @return AjaxResponse
     */
    public function deleteUserFavorite(T2CFavorite $favorite, T2CFavoriteCrudManager $t2CFavoriteCrudManager, UserSearchProvider $userSearchProvider)
    {
        if ($msgPhpUser = $this->getUser()) {
            $user = $userSearchProvider->find($msgPhpUser->getUserId());
            if ($user === $favorite->getUser() && $t2CFavoriteCrudManager->deleteFromRest($favorite)) {
                return (new AjaxResponse(null, Response::HTTP_OK, null, null, 'success', 'Le favori a bien été supprimé'))->formatResponseContent();
            }
        }
        return (new AjaxResponse(null, Response::HTTP_FORBIDDEN, null, null, 'success', 'Le favori n\'a pas été supprimé'))->formatResponseContent();
    }
}