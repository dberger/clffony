<?php

namespace App\Controller\FrontOffice;

use App\Entity\User\User;
use App\Model\Response\AjaxResponse;
use App\Services\RemoteIO\CurlRequestBuilder;
use App\Services\RemoteIO\CurlRequestFormatter;
use App\Services\RemoteIO\CurlRequestQuery;
use App\Services\T2CRoute\T2CRouteFormBuilder;
use App\Services\T2CRoute\T2CRouteFormHandler;
use App\Services\User\UserSearchProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * HomePage
     *
     * @Route("/", name="homepage", methods={"GET"})
     * @return Response
     */
    public function homeAction()
    {
        return $this->render('frontOffice/pages/home.html.twig');
    }

    /**
     * HomePage Ajax Block
     *
     * @Route("/ajax/home", name="ajax_homepage", methods={"GET", "POST"})
     * @param Request $request
     * @param T2CRouteFormHandler $t2CRouteFormHandler
     * @param T2CRouteFormBuilder $t2CRouteFormBuilder
     * @param UserSearchProvider $userSearchProvider
     * @param CurlRequestQuery $curlRequestQuery
     * @param CurlRequestBuilder $curlRequestBuilder
     * @param CurlRequestFormatter $curlRequestFormatter
     * @return Response
     * @throws \Exception
     */
    public function homeAjaxBlockAction(
        Request $request,
        T2CRouteFormHandler $t2CRouteFormHandler,
        T2CRouteFormBuilder $t2CRouteFormBuilder,
        UserSearchProvider $userSearchProvider,
        CurlRequestQuery $curlRequestQuery,
        CurlRequestBuilder $curlRequestBuilder,
        CurlRequestFormatter $curlRequestFormatter
    ) {

        /** @var User $user */
        $user = $this->getUser() ? $userSearchProvider->find($this->getUser()->getUserId()) : null;
        $form = $t2CRouteFormBuilder->getForm($this->generateUrl('ajax_homepage'), $user);

        if ($search = $t2CRouteFormHandler->handle($form, $request)) {
            $data = $curlRequestQuery->query($curlRequestBuilder->findRoutesFromNow($search));
            $routes = $curlRequestFormatter->convertResultsToRouteObject($data, $search);

            if ($routes) {
                return $this->render('frontOffice/ajaxBlocks/stops_from_now.html.twig', [
                    'routes' => $routes,
                    'now' => new \DateTime()
                ]);
            }

            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, []);
            $ajaxResponse
                ->setFlashType('error')
                ->setFlashMessage('Aucun résultat trouvé !');
            return $ajaxResponse->formatResponseContent();
        }

        return $this->render('frontOffice/ajaxBlocks/home_search.html.twig', ['form' => $form->createView()]);
    }
}
