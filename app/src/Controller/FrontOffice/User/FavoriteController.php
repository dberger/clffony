<?php


namespace App\Controller\FrontOffice\User;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Model\Response\AjaxResponse;
use App\Repository\UserRepository;
use App\Services\T2CFavorite\T2CFavoriteFormBuilder;
use App\Services\T2CFavorite\T2CFavoriteFormHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class FavoriteController
 * @package App\Controller\FrontOffice\User
 * @Security("is_granted('ROLE_USER')")
 * @Route("favorite")
 */
class FavoriteController extends AbstractController
{
    /**
     * Add favorite form
     *
     * @Route("/add/{start}/{end}", name="add_favorite", methods={"GET", "POST"})
     * @param T2CStop $start
     * @param T2CStop $end
     * @param Request $request
     * @param T2CFavoriteFormBuilder $t2CFavoriteFormBuilder
     * @param T2CFavoriteFormHandler $t2CFavoriteFormHandler
     * @param UserRepository $userRepository
     * @return Response
     */
    public function addFavoriteAction(
        Request $request,
        T2CFavoriteFormBuilder $t2CFavoriteFormBuilder,
        T2CFavoriteFormHandler $t2CFavoriteFormHandler,
        UserRepository $userRepository,
        T2CStop $start = null,
        T2CStop $end = null
    ) {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        $form = $t2CFavoriteFormBuilder->getFavoriteForm(
            $start,
            $end,
            $this->generateUrl('add_favorite', [
                'start' => $start instanceof T2CStop ? $start->getId() : null,
                'end' => $end instanceof T2CStop ? $end->getId() : null
            ])
        );

        if ($favorite = $t2CFavoriteFormHandler->handle($form, $request, $userRepository->find($this->getUser()->getUserId()))) {
            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL, AjaxResponse::JS_FUNCTION_RELOAD_BLOCK_AJAX], [[], '#home_banner_search']);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Le favori a bien été ajouté');

            return $ajaxResponse->formatResponseContent();
        }

        return $this->render('frontOffice/modals/add_favorite.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Get a json object from a specified t2cfavorite id
     * @IsGranted("ROLE_USER")
     * @Route("/get/{id}", name="ajax_get_favorite", methods={"GET"}, defaults={"id" : null})
     * @param Request $request
     * @param T2CFavorite $t2CFavorite
     * @return JsonResponse
     */
    public function ajaxGetOneFavoriteJsonDataAction(Request $request, T2CFavorite $t2CFavorite = null)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        if (!empty($t2CFavorite) && $t2CFavorite->getUser()->getId() == $this->getUser()->getUserId()) {
            return new JsonResponse($t2CFavorite->getStopInfos());
        }

        throw new AccessDeniedHttpException();
    }
}
