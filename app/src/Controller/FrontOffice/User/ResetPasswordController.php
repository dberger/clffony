<?php

declare(strict_types=1);

namespace App\Controller\FrontOffice\User;

use App\Entity\User\User;
use App\Model\Response\AjaxResponse;
use App\Services\ResetPassword\ResetPasswordFormBuilder;
use App\Services\ResetPassword\ResetPasswordFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @Route("/reset-password/{token}", name="reset_password")
 */
final class ResetPasswordController extends AbstractController
{
    public function __invoke(
        string $token,
        Request $request,
        ResetPasswordFormBuilder $resetPasswordFormBuilder,
        FlashBagInterface $flashBag,
        Environment $twig,
        ResetPasswordFormHandler $resetPasswordFormHandler,
        EntityManagerInterface $em
    ): Response {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        $user = $em->getRepository(User::class)->findOneBy(['passwordResetToken' => $token]);

        if (!$user instanceof User) {
            throw new NotFoundHttpException();
        }

        $form = $resetPasswordFormBuilder->getResetPasswordForm($this->generateUrl('reset_password'), $token);

        if ($resetPasswordFormHandler->handle($form, $request, $user)) {
            $flashBag->add('success', 'Votre mot de passe a bien été changé.');

            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL]);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Votre mot de passe a bien été changé');

            return $ajaxResponse->formatResponseContent();
        }

        return new Response($twig->render('frontOffice/user/reset_password.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}
