<?php

declare(strict_types=1);

namespace App\Controller\FrontOffice\User;

use App\Model\Response\AjaxResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @Route("/profile", name="profile")
 */
final class ProfileController
{
    /**
     * @param Request $request
     * @param Environment $twig
     * @return JsonResponse
     */
    public function __invoke(Request $request, Environment $twig): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL, AjaxResponse::JS_FUNCTION_RELOAD_PAGE]);
        $ajaxResponse
            ->setFlashType('success')
            ->setFlashMessage('Connection réussi');

        return $ajaxResponse->formatResponseContent();
    }
}
