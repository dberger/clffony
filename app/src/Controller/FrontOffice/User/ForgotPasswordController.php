<?php

declare(strict_types=1);

namespace App\Controller\FrontOffice\User;

use App\Model\Response\AjaxResponse;
use App\Services\ForgotPassword\ForgotPasswordFormBuilder;
use App\Services\ForgotPassword\ForgotPasswordFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route("/forgot-password", name="forgot_password")
 */
final class ForgotPasswordController extends AbstractController
{
    /**
     * @param Request $request
     * @param ForgotPasswordFormBuilder $forgotPasswordFormBuilder
     * @param FlashBagInterface $flashBag
     * @param Environment $twig
     * @param ForgotPasswordFormHandler $forgotPasswordFormHandler
     * @param EntityManagerInterface $em
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(
        Request $request,
        ForgotPasswordFormBuilder $forgotPasswordFormBuilder,
        FlashBagInterface $flashBag,
        Environment $twig,
        ForgotPasswordFormHandler $forgotPasswordFormHandler,
        EntityManagerInterface $em
    ): Response {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        $form = $forgotPasswordFormBuilder->getForgotPasswordForm($this->generateUrl('forgot_password'));

        if ($forgotPasswordFormHandler->handle($form, $request)) {
            $flashBag->add('success', 'Un email a été envoyé pour récupérer votre mot de passe.');

            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL]);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Un email a été envoyé pour récupérer votre mot de passe');

            return $ajaxResponse->formatResponseContent();
        }

        return new Response($twig->render('frontOffice/user/forgot_password.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}
