<?php

declare(strict_types=1);

namespace App\Controller\FrontOffice\User;

use App\Model\Response\AjaxResponse;
use App\Services\Register\RegisterFormBuilder;
use App\Services\Register\RegisterFormHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route("/register", name="register")
 */
final class RegisterController extends AbstractController
{
    /**
     * @param Request $request
     * @param RegisterFormBuilder $registerFormBuilder
     * @param FlashBagInterface $flashBag
     * @param Environment $twig
     * @param RegisterFormHandler $registerFormHandler
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(
        Request $request,
        RegisterFormBuilder $registerFormBuilder,
        FlashBagInterface $flashBag,
        Environment $twig,
        RegisterFormHandler $registerFormHandler
    ): Response {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        $form = $registerFormBuilder->getRegisterForm($this->generateUrl('register'));

        if ($registerFormHandler->handle($form, $request)) {
            $flashBag->add('success', 'Vous êtes maintenant inscrit.');

            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL]);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Vous êtes maintenant inscrit');

            return $ajaxResponse->formatResponseContent();
        }

        return new Response($twig->render('frontOffice/user/register.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}
