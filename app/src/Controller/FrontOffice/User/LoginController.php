<?php

declare(strict_types=1);

namespace App\Controller\FrontOffice\User;

use App\Services\Login\LoginFormBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route("/login", name="login")
 */
final class LoginController extends AbstractController
{
    /**
     * @param Request $request
     * @param Environment $twig
     * @param LoginFormBuilder $loginFormBuilder
     * @param AuthenticationUtils $authenticationUtils
     * @return JsonResponse|Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(
        Request $request,
        Environment $twig,
        LoginFormBuilder $loginFormBuilder,
        AuthenticationUtils $authenticationUtils
    ) {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }

        $form = $loginFormBuilder->getLoginForm($this->generateUrl('login'));

        if (null !== $error = $authenticationUtils->getLastAuthenticationError(true)) {
            $form->addError(new FormError($error->getMessage(), $error->getMessageKey(), $error->getMessageData()));
        }

        return new Response($twig->render('frontOffice/user/login.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}
