<?php

namespace App\Controller\BackOffice;

use App\Services\Import\T2C\T2CUpdater;
use App\Services\T2CStop\T2CStopDataTableLoader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class T2CStopController
 * @package App\Controller\BackOffice
 * @Route("admin")
 */
class T2CStopController extends AbstractController
{
    /**
     * T2CStop Page BackOffice
     *
     * @Route("/stop", name="admin_stop_page", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function stopListAction(Request $request)
    {
        return $this->render('backOffice/pages/stop/list.html.twig');
    }

    /**
     * @Route("/ajax/stop/getAll", name="admin_ajax_get_all_stops", methods={"GET"})
     * @param Request $request
     * @param T2CStopDataTableLoader $t2CStopDataTableLoader
     * @return JsonResponse
     */
    public function ajaxGetStopsDataAction(Request $request, T2CStopDataTableLoader $t2CStopDataTableLoader)
    {
        return new JsonResponse($t2CStopDataTableLoader->getAllStops());
    }

    /**
     * @Route("/ajax/stop/update", name="admin_ajax_update_stops", methods={"GET"})
     * @param T2CUpdater $t2CUpdater
     * @return Response
     */
    public function ajaxReloadAllStopsFromRemoteAction(T2CUpdater $t2CUpdater)
    {
        $updatedData = $t2CUpdater->updateDatabase();
        return $this->render('backOffice/pages/stop/_updated_stop.html.twig', ['stops' => $updatedData]);
    }
}
