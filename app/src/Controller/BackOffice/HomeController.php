<?php

namespace App\Controller\BackOffice;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller\BackOffice
 * @Security("is_granted('ROLE_ADMIN')")
 * @Route("admin")
 */
class HomeController extends AbstractController
{
    /**
     * HomePage BackOffice
     *
     * @Route("/", name="admin_homepage", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function homeAction(Request $request)
    {
        return $this->render('backOffice/base/layout.html.twig');
    }
}
