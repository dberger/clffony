<?php

namespace App\Controller\BackOffice;

use App\Entity\User\User;
use App\Model\Response\AjaxResponse;
use App\Services\User\UserCrudManager;
use App\Services\User\UserDataTableLoader;
use App\Services\User\UserFormBuilder;
use App\Services\User\UserFormHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\BackOffice
 * @Security("is_granted('ROLE_ADMIN')")
 * @Route("admin")
 */
class UserController extends AbstractController
{
    /**
     * HomePage BackOffice
     *
     * @Route("/user", name="admin_userpage", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function userListAction(Request $request)
    {
        return $this->render('backOffice/pages/user/list.html.twig');
    }

    /**
     * @Route("/ajax/user/getAll", name="admin_ajax_get_all", methods={"GET"})
     * @param UserDataTableLoader $userDataTableLoader
     * @return JsonResponse
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function ajaxGetUsersData(UserDataTableLoader $userDataTableLoader)
    {
        return new JsonResponse($userDataTableLoader->getAllUsers());
    }

    /**
     * HomePage BackOffice - Add user
     *
     * @Route("/user/add", name="admin_userpage_add", methods={"GET", "POST"})
     * @param Request $request
     * @param UserFormBuilder $userFormBuilder
     * @param UserFormHandler $userFormHandler
     * @return Response
     */
    public function addUserAction(Request $request, UserFormBuilder $userFormBuilder, UserFormHandler $userFormHandler)
    {
        $form = $userFormBuilder->getForm($this->generateUrl('admin_userpage_add'));

        if ($userFormHandler->handle($form, $request)) {
            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL, AjaxResponse::JS_FUNCTION_RELOAD_DATATABLE]);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Succès');

            return $ajaxResponse->formatResponseContent();
        }

        return $this->render('backOffice/pages/user/ajax/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * HomePage BackOffice - Edit user
     *
     * @Route("/user/edit/{id}", name="admin_userpage_edit", methods={"GET", "POST"})
     * @param Request $request
     * @param User $user
     * @param UserFormBuilder $userFormBuilder
     * @param UserFormHandler $userFormHandler
     * @return Response
     */
    public function editUserAction(Request $request, User $user, UserFormBuilder $userFormBuilder, UserFormHandler $userFormHandler)
    {
        $form = $userFormBuilder->getEditForm($this->generateUrl('admin_userpage_edit', ['id' => $user->getId()]), $user);

        if ($userFormHandler->handleEdit($form, $request, $user)) {
            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_CLOSE_MODAL, AjaxResponse::JS_FUNCTION_RELOAD_DATATABLE]);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Succès');

            return $ajaxResponse->formatResponseContent();
        }

        return $this->render('backOffice/pages/user/ajax/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * HomePage BackOffice - Delete user
     *
     * @Route("/user/delete/{id}", name="admin_userpage_delete", methods={"GET"})
     * @param Request $request
     * @param User $user
     * @param UserCrudManager $userCrudManager
     * @return Response
     */
    public function deleteUserAction(Request $request, User $user, UserCrudManager $userCrudManager)
    {
        if ($userCrudManager->delete($user, $request->get('token'))) {
            $ajaxResponse = new AjaxResponse(null, Response::HTTP_OK, [AjaxResponse::JS_FUNCTION_RELOAD_DATATABLE]);
            $ajaxResponse
                ->setFlashType('success')
                ->setFlashMessage('Succès');

            return $ajaxResponse->formatResponseContent();
        };
    }
}
