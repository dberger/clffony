<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class T2CRouteExtension
 * @package App\Twig
 */
class T2CRouteExtension extends AbstractExtension
{
    /*
     * Get functions
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('substractDate', [$this, 'substractDate']),
        ];
    }

    /**
     * @param \DateTime $nextTram
     * @param \DateTime $now
     * @return string
     */
    public function substractDate(\DateTime $nextTram, \DateTime $now)
    {
        return $nextTram->diff($now);
    }
}
