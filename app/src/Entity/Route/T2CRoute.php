<?php

namespace App\Entity\Route;

use App\Entity\Stops\BaseStop;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="clf_routes",
 *     options={"comment":"Table of routes"}
 * )
 */
class T2CRoute
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Id of route"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", options={"comment":"Route created at"})
     */
    private $createdAt;

    /**
     * @var BaseStop
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity="App\Entity\Stops\BaseStop")
     * @ORM\JoinColumn(name="id_stop_start", referencedColumnName="id")
     */
    private $stopStart;

    /**
     * @var BaseStop
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity="App\Entity\Stops\BaseStop")
     * @ORM\JoinColumn(name="id_stop_end", referencedColumnName="id")
     */
    private $stopEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", options={"comment":"Hours of begin"})
     */
    private $hourStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", options={"comment":"Hours of finish line"})
     */
    private $hourEnd;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return BaseStop
     */
    public function getStopStart(): BaseStop
    {
        return $this->stopStart;
    }

    /**
     * @param BaseStop $stopStart
     * @return T2CRoute
     */
    public function setStopStart(BaseStop $stopStart): T2CRoute
    {
        $this->stopStart = $stopStart;
        return $this;
    }

    /**
     * @return BaseStop
     */
    public function getStopEnd(): BaseStop
    {
        return $this->stopEnd;
    }

    /**
     * @param BaseStop $stopEnd
     * @return T2CRoute
     */
    public function setStopEnd(BaseStop $stopEnd): T2CRoute
    {
        $this->stopEnd = $stopEnd;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourStart(): ?\DateTime
    {
        return $this->hourStart;
    }

    /**
     * @param \DateTime $hourStart
     * @return T2CRoute
     */
    public function setHourStart(\DateTime $hourStart): T2CRoute
    {
        $this->hourStart = $hourStart;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourEnd(): ?\DateTime
    {
        return $this->hourEnd;
    }

    /**
     * @param \DateTime $hourEnd
     * @return T2CRoute
     */
    public function setHourEnd(\DateTime $hourEnd): T2CRoute
    {
        $this->hourEnd = $hourEnd;
        return $this;
    }
}
