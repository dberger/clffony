<?php

namespace App\Entity\Lines;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class T2CLine
 * @package App\Entity\Lines
 * @ORM\Entity(repositoryClass="App\Repository\T2CLineRepository")
 */
class T2CLine extends BaseLine
{
    const T2C_TYPE_TRAM = 'tram';
    const T2C_TYPE_BUS = 'bus';

    /**
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Type of the line"})
     */
    private $type;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return T2CLine
     */
    public function setType(string $type): T2CLine
    {
        $this->type = $type;
        return $this;
    }
}
