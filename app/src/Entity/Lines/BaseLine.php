<?php

namespace App\Entity\Lines;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Stops\BaseStop;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="clf_lines",
 *     options={"comment":"Table of stops"}
 * )
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "BaseLine",
 *     "t2c_line" = "T2CLine",
 *     })
 */
class BaseLine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Id of stop"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull
     * @ORM\Column(type="string", length=255, options={"comment":"Name of Stop"})
     */
    private $name;

    /**
     * @var BaseStop[]
     * @ORM\OneToMany(targetEntity="App\Entity\Stops\BaseStop", mappedBy="line", cascade={"persist", "remove"})
     */
    private $stops;

    /**
     * BaseLine constructor.
     */
    public function __construct()
    {
        $this->stops = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BaseLine
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BaseLine
     */
    public function setName(string $name): BaseLine
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return BaseStop[]
     */
    public function getStops()
    {
        return $this->stops;
    }

    /**
     * @param BaseStop[] $stops
     * @return BaseLine
     */
    public function setStops(array $stops): BaseLine
    {
        $this->stops = $stops;
        return $this;
    }

    /**
     * Add a stop
     * @param BaseStop $baseStop
     * @return BaseLine
     */
    public function addStop(BaseStop $baseStop)
    {
        $this->stops->add($baseStop);

        return $this;
    }

    /**
     * Remove a stop
     * @param BaseStop $baseStop
     * @return BaseLine
     */
    public function removeStop(BaseStop $baseStop)
    {
        if ($this->stops->contains($baseStop)) {
            $this->stops->removeElement($baseStop);
        }
        return $this;
    }
}
