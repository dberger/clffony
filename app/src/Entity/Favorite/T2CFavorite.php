<?php

namespace App\Entity\Favorite;

use App\Entity\Stops\BaseStop;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\T2CFavoriteRepository")
 * @ORM\Table(name="t2c_favorites",
 *     options={"comment":"Table of favorites"}
 * )
 */
class T2CFavorite
{
    const KEY_START = 'start';
    const KEY_STOP = 'stop';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Id of route"})
     */
    private $id;

    /**
     * @var BaseStop
     * @ORM\ManyToOne(targetEntity="App\Entity\Stops\BaseStop")
     * @ORM\JoinColumn(name="id_stop_start", referencedColumnName="id")
     */
    private $stopStart;

    /**
     * @var BaseStop
     * @ORM\ManyToOne(targetEntity="App\Entity\Stops\BaseStop")
     * @ORM\JoinColumn(name="id_stop_end", referencedColumnName="id")
     */
    private $stopEnd;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="favorites")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     * @Assert\NotNull
     * @ORM\Column(type="string", options={"comment":"Name of the favorite"})
     */
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return T2CFavorite
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return BaseStop
     */
    public function getStopStart(): ?BaseStop
    {
        return $this->stopStart;
    }

    /**
     * @param BaseStop $stopStart
     * @return T2CFavorite
     */
    public function setStopStart(BaseStop $stopStart): T2CFavorite
    {
        $this->stopStart = $stopStart;
        return $this;
    }

    /**
     * @return BaseStop
     */
    public function getStopEnd(): ?BaseStop
    {
        return $this->stopEnd;
    }

    /**
     * @param BaseStop $stopEnd
     * @return T2CFavorite
     */
    public function setStopEnd(BaseStop $stopEnd): T2CFavorite
    {
        $this->stopEnd = $stopEnd;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return T2CFavorite
     */
    public function setUser(User $user): T2CFavorite
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return T2CFavorite
     */
    public function setName(string $name): T2CFavorite
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getStopInfos()
    {
        return [self::KEY_START => $this->stopStart->getId(), self::KEY_STOP => $this->stopEnd->getId()];
    }
}
