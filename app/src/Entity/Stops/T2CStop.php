<?php

namespace App\Entity\Stops;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class T2CStop
 * @package App\Entity\Stops
 * @ORM\Entity(repositoryClass="App\Repository\T2CStopRepository")
 */
class T2CStop extends BaseStop
{
    const T2C_CITY_CLF = 'Clermond-Ferrand';
    const T2C_CITY_AUBIERE = 'Aubiere';

    /**
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"City of the stop"})
     */
    private $city;

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return T2CStop
     */
    public function setCity(string $city): T2CStop
    {
        $this->city = $city;
        return $this;
    }
}
