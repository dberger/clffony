<?php

namespace App\Entity\Stops;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Lines\BaseLine;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="clf_stops",
 *     options={"comment":"Table of stops"}
 * )
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "BaseStop",
 *     "t2c_stop" = "T2CStop",
 *     })
 */
class BaseStop
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Id of stop"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull
     * @ORM\Column(type="string", length=255, options={"comment":"Name of Stop"})
     */
    private $name;

    /**
     * @var BaseLine
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity="App\Entity\Lines\BaseLine", inversedBy="stops")
     * @ORM\JoinColumn(name="id_line", referencedColumnName="id")
     */
    private $line;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BaseStop
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BaseStop
     */
    public function setName(string $name): BaseStop
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return BaseLine
     */
    public function getLine(): BaseLine
    {
        return $this->line;
    }

    /**
     * @param BaseLine $line
     * @return BaseStop
     */
    public function setLine(BaseLine $line): BaseStop
    {
        $this->line = $line;
        return $this;
    }
}
