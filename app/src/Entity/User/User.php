<?php

namespace App\Entity\User;

use App\Entity\Favorite\T2CFavorite;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use MsgPhp\User\Entity\User as BaseUser;
use MsgPhp\User\UserIdInterface;
use MsgPhp\Domain\Event\DomainEventHandlerInterface;
use MsgPhp\Domain\Event\DomainEventHandlerTrait;
use MsgPhp\User\Entity\Credential\EmailPassword;
use MsgPhp\User\Entity\Features\EmailPasswordCredential;
use MsgPhp\User\Entity\Features\ResettablePassword;
use MsgPhp\User\Entity\Fields\RolesField;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser implements DomainEventHandlerInterface, UserInterface
{
    use DomainEventHandlerTrait;
    use EmailPasswordCredential;
    use ResettablePassword;
    use RolesField;

    /** @ORM\Id() @ORM\GeneratedValue() @ORM\Column(type="msgphp_user_id", length=191) */
    private $id;

    /**
     * @var T2CFavorite[]
     * @ORM\OneToMany(targetEntity="App\Entity\Favorite\T2CFavorite", mappedBy="user", cascade={"persist", "remove"})
     */
    private $favorites;

    public function __construct(UserIdInterface $id, string $email, string $password)
    {
        $this->id = $id;
        $this->credential = new EmailPassword($email, $password);
        $this->favorites = new ArrayCollection();
    }

    public function getId(): UserIdInterface
    {
        return $this->id;
    }

    /**
     * @return T2CFavorite[]
     */
    public function getFavorites(): array
    {
        return $this->favorites;
    }

    /**
     * Add a favorite
     * @param T2CFavorite $t2cFavorite
     * @return $this
     */
    public function addFavorite(T2CFavorite $t2cFavorite)
    {
        if (!$this->favorites->contains($t2cFavorite)) {
            $this->favorites->add($t2cFavorite);
        }
        return $this;
    }

    /**
     * Remove a favorite
     * @param T2CFavorite $t2cFavorite
     * @return $this
     */
    public function removeFavorite(T2CFavorite $t2cFavorite)
    {
        if ($this->favorites->contains($t2cFavorite)) {
            $this->favorites->removeElement($t2cFavorite);
        }
        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
