<?php

namespace App\Form;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Model\T2CFavorite\T2CFavoriteSearch;
use App\Model\T2CRoute\T2CRouteSearch;
use App\Repository\T2CFavoriteRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class T2CRouteSearchType
 * @package App\Form
 */
class T2CRouteSearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stopStart', EntityType::class, [
                'label' => 'Arrêt de départ',
                'class' => T2CStop::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r');
                },
                'choice_label' => 'name',
                'placeholder' => 'Choisir un arrêt de départ',
                'attr' => ['class' => 'select2 select-start'],
            ])
            ->add('stopEnd', EntityType::class, [
                'label' => 'Arrêt d\'arrivé',
                'class' => T2CStop::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r');
                },
                'choice_label' => 'name',
                'placeholder' => 'Choisir un arrêt d\'arrivé',
                'attr' => ['class' => 'select2 select-end'],
            ])
            ->add('maxResults', IntegerType::class, [
                'label' => 'Nombre de résultats',
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'class' => 'search-max-result'
                ],
                'data' => 5
            ])
           ;
        if (($user = $options['user']) != null) {
            $builder->add('t2cFavorite', EntityType::class, [
                'label' => 'Mes favoris',
                'class' => T2CFavorite::class,
                'query_builder' => function (T2CFavoriteRepository $er) use ($user) {
                    return $er->getSearchQuery((new T2CFavoriteSearch())->setUser($user), true);
                },
                'choice_label' => 'name',
                'placeholder' => '- Choisir un favori -',
                'attr' => ['class' => 'select-favorite'],
                'required' => 'false'
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => T2CRouteSearch::class,
            'user' => null
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_t2c_route_search';
    }
}
