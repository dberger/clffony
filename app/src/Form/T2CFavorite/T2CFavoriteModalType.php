<?php

namespace App\Form\T2CFavorite;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class T2CFavoriteModalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var T2CStop $stopStart */
        $stopStart = $options['stopStart'];
        /** @var T2CStop $stopEnd */
        $stopEnd = $options['stopEnd'];

        $builder
            ->add('name', null, [
                'label' => 'Nom du favori',
                'required' => true,
            ])
            ->add('stopStart', EntityType::class, [
                'label' => 'Arrêt de départ',
                'class' => T2CStop::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r');
                },
                'choice_label' => 'name',
                'placeholder' => 'Choisir un arrêt de départ',
                'attr' => ['class' => 'select2'],
                'data' => $stopStart
            ])
            ->add('stopEnd', EntityType::class, [
                'label' => 'Arrêt d\'arrivé',
                'class' => T2CStop::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r');
                },
                'choice_label' => 'name',
                'placeholder' => 'Choisir un arrêt d\'arrivé',
                'attr' => ['class' => 'select2'],
                'data' => $stopEnd
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => T2CFavorite::class
        ])->setRequired(['stopStart', 'stopEnd']);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_t2c_favorite';
    }
}
