<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Temporary fix for the index
 */
final class Version20190124170324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Temporary fix for the index';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX discr_idx ON clf_lines');
        $this->addSql('DROP INDEX discr_idx ON clf_stops');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX discr_idx ON clf_lines (discr(191))');
        $this->addSql('CREATE INDEX discr_idx ON clf_stops (discr(191))');
    }
}
