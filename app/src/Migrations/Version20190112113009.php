<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add t2croute entity
 */
final class Version20190112113009 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE clf_routes (id INT AUTO_INCREMENT NOT NULL COMMENT \'Id of route\', id_stop_start INT DEFAULT NULL COMMENT \'Id of stop\', id_stop_end INT DEFAULT NULL COMMENT \'Id of stop\', created_at DATETIME NOT NULL COMMENT \'Route created at\', INDEX IDX_2303F6C7B7113E6F (id_stop_start), INDEX IDX_2303F6C763A2F9A0 (id_stop_end), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB COMMENT = \'Table of routes\' ');
        $this->addSql('ALTER TABLE clf_routes ADD CONSTRAINT FK_2303F6C7B7113E6F FOREIGN KEY (id_stop_start) REFERENCES clf_stops (id)');
        $this->addSql('ALTER TABLE clf_routes ADD CONSTRAINT FK_2303F6C763A2F9A0 FOREIGN KEY (id_stop_end) REFERENCES clf_stops (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE clf_routes');
    }
}
