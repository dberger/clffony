<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * add admin account
 */
final class Version20190319204322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add admin account';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO user (id, credential_email, credential_password) VALUES ('', 'admin@clffony.fr', '$2y$13$/AHp.unG.GDmLk5wZUH26ezyzPpDZKgy7e5ScAU6ATsruD18lAGju')");
        $this->addSql("INSERT INTO user_role (user_id, role_name) VALUES (1, 'ROLE_ADMIN')");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('SET FOREIGN_KEY_CHECKS=0;');
        $this->addSql("TRUNCATE table user");
        $this->addSql("DELETE FROM user_role WHERE user_id = 1");
        $this->addSql('SET FOREIGN_KEY_CHECKS=1;');
    }
}
