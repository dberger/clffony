<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add base entities
 */
final class Version20190111155824 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE clf_lines (id INT AUTO_INCREMENT NOT NULL COMMENT \'Id of stop\', name VARCHAR(255) NOT NULL COMMENT \'Name of Stop\', discr VARCHAR(255) NOT NULL, type VARCHAR(255) DEFAULT NULL COMMENT \'Type of the line\', INDEX discr_idx (discr), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB COMMENT = \'Table of stops\' ');
        $this->addSql('CREATE TABLE clf_stops (id INT AUTO_INCREMENT NOT NULL COMMENT \'Id of stop\', id_line INT DEFAULT NULL COMMENT \'Id of stop\', name VARCHAR(255) NOT NULL COMMENT \'Name of Stop\', discr VARCHAR(255) NOT NULL, city VARCHAR(255) DEFAULT NULL COMMENT \'City of the stop\', INDEX IDX_EE8AA77F5A34A8F4 (id_line), INDEX discr_idx (discr), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB COMMENT = \'Table of stops\' ');
        $this->addSql('ALTER TABLE clf_stops ADD CONSTRAINT FK_EE8AA77F5A34A8F4 FOREIGN KEY (id_line) REFERENCES clf_lines (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE clf_stops DROP FOREIGN KEY FK_EE8AA77F5A34A8F4');
        $this->addSql('DROP TABLE clf_lines');
        $this->addSql('DROP TABLE clf_stops');
    }
}
