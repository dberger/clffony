<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\User\Role;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Init user roles
 */
final class Version20190205195909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Init user roles';
    }

    public function up(Schema $schema): void
    {
        foreach (Role::getRoles() as $role) {
            $this->addSql("INSERT INTO role (name) VALUES ('{$role}')");
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql("TRUNCATE TABLE role");
    }
}
