<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add user favorites
 */
final class Version20190128121937 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add user favorites';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE t2c_favorites (id INT AUTO_INCREMENT NOT NULL COMMENT \'Id of route\', id_stop_start INT DEFAULT NULL COMMENT \'Id of stop\', id_stop_end INT DEFAULT NULL COMMENT \'Id of stop\', id_user INT DEFAULT NULL COMMENT \'(DC2Type:msgphp_user_id)\', INDEX IDX_E62A1ACB7113E6F (id_stop_start), INDEX IDX_E62A1AC63A2F9A0 (id_stop_end), INDEX IDX_E62A1AC6B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB COMMENT = \'Table of favorites\' ');
        $this->addSql('ALTER TABLE t2c_favorites ADD CONSTRAINT FK_E62A1ACB7113E6F FOREIGN KEY (id_stop_start) REFERENCES clf_stops (id)');
        $this->addSql('ALTER TABLE t2c_favorites ADD CONSTRAINT FK_E62A1AC63A2F9A0 FOREIGN KEY (id_stop_end) REFERENCES clf_stops (id)');
        $this->addSql('ALTER TABLE t2c_favorites ADD CONSTRAINT FK_E62A1AC6B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE t2c_favorites');
    }
}
