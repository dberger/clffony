<?php

namespace App\DataFixtures;

use App\DataProviders\UserFactory;
use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Services\T2CStop\T2CStopSearchProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use MsgPhp\User\UserId;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends Fixture
{

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var T2CStopSearchProvider
     */
    private $t2CStopSearchProvider;

    /**
     * UserFixtures constructor.
     * @param UserFactory $userFactory
     * @param T2CStopSearchProvider $t2CStopSearchProvider
     */
    public function __construct(UserFactory $userFactory, T2CStopSearchProvider $t2CStopSearchProvider)
    {
        $this->userFactory = $userFactory;
        $this->t2CStopSearchProvider = $t2CStopSearchProvider;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        /** @var T2CStop[] $stops */
        $stops = $this->t2CStopSearchProvider->findAll();
        $nbStops = count($stops);
        for ($i = 0; $i < 10; $i++) {
            $user = $this->userFactory->createUser(new UserId(), $faker->email, 'user');
            if ($nbStops > 0) {
                for ($x = 0; $x < 5; $x++) {
                    $user->addFavorite(
                        (new T2CFavorite())
                            ->setUser($user)
                            ->setStopStart($stops[rand(0, $nbStops - 1)])
                            ->setStopEnd($stops[rand(0, $nbStops - 1)])
                            ->setName($faker->company)
                    );
                }
            }
            $manager->persist($user);
        }

        $manager->flush();
    }
}
