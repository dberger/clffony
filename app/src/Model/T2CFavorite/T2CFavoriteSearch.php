<?php

namespace App\Model\T2CFavorite;

use App\Entity\Stops\T2CStop;
use App\Entity\User\User;
use Symfony\Component\Validator\Constraints as Assert;

class T2CFavoriteSearch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotNull
     * @var T2CStop
     */
    private $stopStart;

    /**
     * @Assert\NotNull
     * @var T2CStop
     */
    private $stopEnd;

    /**
     * @Assert\NotNull
     * @var User
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return T2CFavoriteSearch
     */
    public function setId(int $id): T2CFavoriteSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return T2CStop
     */
    public function getStopStart(): ?T2CStop
    {
        return $this->stopStart;
    }

    /**
     * @param T2CStop $stopStart
     * @return T2CFavoriteSearch
     */
    public function setStopStart(T2CStop $stopStart): T2CFavoriteSearch
    {
        $this->stopStart = $stopStart;
        return $this;
    }

    /**
     * @return T2CStop
     */
    public function getStopEnd(): ?T2CStop
    {
        return $this->stopEnd;
    }

    /**
     * @param T2CStop $stopEnd
     * @return T2CFavoriteSearch
     */
    public function setStopEnd(T2CStop $stopEnd): T2CFavoriteSearch
    {
        $this->stopEnd = $stopEnd;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return T2CFavoriteSearch
     */
    public function setUser(?User $user): T2CFavoriteSearch
    {
        $this->user = $user;
        return $this;
    }
}
