<?php

namespace App\Model\T2CRoute;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use Symfony\Component\Validator\Constraints as Assert;

class T2CRouteSearch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\GreaterThanOrEqual(value = 0)
     * @var integer
     */
    private $maxResults;

    /**
     * @Assert\NotNull
     * @var T2CStop
     */
    private $stopStart;

    /**
     * @Assert\NotNull
     * @var T2CStop
     */
    private $stopEnd;

    /**
     * @var T2CFavorite
     */
    private $t2cFavorite;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return T2CRouteSearch
     */
    public function setId(int $id): T2CRouteSearch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return T2CStop
     */
    public function getStopStart(): ?T2CStop
    {
        return $this->stopStart;
    }

    /**
     * @param T2CStop $stopStart
     * @return T2CRouteSearch
     */
    public function setStopStart(T2CStop $stopStart): T2CRouteSearch
    {
        $this->stopStart = $stopStart;
        return $this;
    }

    /**
     * @return T2CStop
     */
    public function getStopEnd(): ?T2CStop
    {
        return $this->stopEnd;
    }

    /**
     * @param T2CStop $stopEnd
     * @return T2CRouteSearch
     */
    public function setStopEnd(T2CStop $stopEnd): T2CRouteSearch
    {
        $this->stopEnd = $stopEnd;
        return $this;
    }

    /**
     * @return T2CFavorite
     */
    public function getT2cFavorite(): ?T2CFavorite
    {
        return $this->t2cFavorite;
    }

    /**
     * @param T2CFavorite $t2cFavorite
     * @return T2CRouteSearch
     */
    public function setT2cFavorite(T2CFavorite $t2cFavorite): T2CRouteSearch
    {
        $this->t2cFavorite = $t2cFavorite;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxResults(): ?int
    {
        return $this->maxResults;
    }

    /**
     * @param int $maxResults
     * @return T2CRouteSearch
     */
    public function setMaxResults(?int $maxResults): T2CRouteSearch
    {
        $this->maxResults = $maxResults;
        return $this;
    }
}
