# Changelog

Tous les changements notables à  ce projet seront documentés dans ce fichier.
Le format est basé sur [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
et ce projet adhère à  la version sémantique. [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.3] 31/05/2019
- On peut maintenant faire une recherche d'itinéraire, consulter ses favoris, se connecter via appels REST.
## [1.0.2] 31/03/2019
- Refactorisation et optimisation de la partie gestion d'utilisateur
## [1.0.1] 30/03/2019
- Correction d'un bug lors de la sélection des favoris sur la page principale
## [1.0.0] 30/03/2019
 - Initial release

